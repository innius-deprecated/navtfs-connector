﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace TI.NavTfsConnector
{
    public class ConnectionFileRepository<T> where T : IConnection, new()
    {
        private string mFolderName = null;

        public ConnectionFileRepository(string folderName) : this()
        {
            mFolderName = folderName;
        }

        public ConnectionFileRepository() {}

        public IConnection Update(IConnection connection)
        {
            return HandleUpdate(connection, true);   
        }

        public IConnection Delete(IConnection connection)
        {
            return HandleUpdate(connection, false);
        }

        private IConnection HandleUpdate(IConnection connection, bool insert)
        {
            var l = Load().ToList();
            var c = l.FirstOrDefault(x => x.Name == connection.Name);
            if (c != null)
            {
                l.Remove(c);
            }
            if (insert)
                l.Add((T)connection);
            SaveUserConnections(l);
            return connection;
        }

        public IEnumerable<T> Load()
        {
            if (File.Exists(GetConnectionStoreFileName()))
            {
                using (var s = new FileStream(GetConnectionStoreFileName(), FileMode.Open))
                    return GetConnectionsFromStream(s);
            }
            return new List<T>();
        }

        private IEnumerable<T> GetConnectionsFromStream(Stream configStream)
        {
            XDocument doc = XDocument.Load(configStream);
            var c = from x in doc.Root.Elements("connection")
                    select new T
                    {
                        Name = x.Attribute("name").Value,                        
                        Database = x.Attribute("database").Value,                        
                        NavClientFolder = x.Attribute("clientfolder").Value,
                        ConnectionString = x.Attribute("connectionstring") == null ? string.Empty : x.Attribute("connectionstring").Value,
                        WorkspaceFolder = x.Attribute("workspacefolder").Value,
                        ObjectNamingConvention = x.Attribute("objectnamingconvention") == null ?
                            NamingConvention.Default : (NamingConvention)Enum.Parse(typeof(NamingConvention), x.Attribute("objectnamingconvention").Value),
                        ProjectCollection = GetProjectCollectionUri(x.Attribute("projectcollection").Value),
                        NamingExpression = x.Attribute("objectnamingexpression") == null ? string.Empty : x.Attribute("objectnamingexpression").Value,
                        Status = ConnectionStatus.Stopped
                    };
            return c;
        }

        
        private void SaveUserConnections(List<T> configurations)
        {
            using (var s = new FileStream(GetConnectionStoreFileName(), FileMode.Create))
            using (var writer = XmlWriter.Create(s, new XmlWriterSettings { }))
            {
                writer.WriteStartElement("connections");
                foreach (var c in configurations)
                {
                    writer.WriteStartElement("connection");
                    writer.WriteAttributeString("name", c.Name);                    
                    writer.WriteAttributeString("database", c.Database);
                    writer.WriteAttributeString("connectionstring", c.ConnectionString);
                    writer.WriteAttributeString("clientfolder", c.NavClientFolder);                    
                    writer.WriteAttributeString("workspacefolder", c.WorkspaceFolder);
                    writer.WriteAttributeString("projectcollection", c.ProjectCollection != null ? c.ProjectCollection.ToString() : "");
                    writer.WriteAttributeString("objectnamingconvention", c.ObjectNamingConvention.ToString());
                    writer.WriteAttributeString("objectnamingexpression", c.NamingExpression != null ? c.NamingExpression.ToString() : string.Empty);
                    writer.WriteEndElement();
                }
            }
        }

        private Uri GetProjectCollectionUri(string uri)
        {
            Uri result;
            if (!string.IsNullOrEmpty(uri))
            {
                if (Uri.TryCreate(uri, UriKind.Absolute, out result))
                    return result;
            }
            return null;
        }

        public string GetConnectionStoreFileName()
        {
            return Path.Combine(!string.IsNullOrEmpty(mFolderName) ? mFolderName : 
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "connections.xml");
        }
    }
}
