﻿using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security;

namespace TI.NavTfsConnector.NavObjectDesigner
{
    internal static class ConnectionExtension
    {
        public static string GetDatabaseName(this IConnection connection)
        {
            SqlConnection conn = new SqlConnection(connection.ConnectionString);
            return conn.Database;
        }
    }

    public class Nav2009 : INavObjectDesigner
    {
        private IObjectDesigner _objectDesigner;
        private const string DefaultMonikerName = "!C/SIDE";
        private const string ObjectDesignerGuid = "50000004-0000-1000-0001-0000836BD2D2";
        private IConnection mConnection;


        public Nav2009(IConnection connection)
        {
            mConnection = connection;
        }

        public void ExportLockedObjects(string fileName)
        {
            Export("SORTING(Field1,Field2,Field3) WHERE(Field40=1(1))", fileName);
        }

        public void Export(string filter, string fileName)
        {
            ConnectToNAV();
            string message;
            using (Stream objStream = File.Open(fileName, FileMode.Create, FileAccess.Write))
            {
                if (!ProcessResult(ReadObjectsToStream(filter, objStream), out message))
                    throw new NavTfsException(message);
            }
        }

        public void Import(IEnumerable<IWorkspaceObject> objects)
        {
            ConnectToNAV();
            string message;  
            foreach (var o in objects)
            {
                using (Stream objStream = File.OpenRead(o.LocalSpec))
                {
                    if (!ProcessResult(WriteObjectFromStream(objStream), out message))
                        throw new NavTfsObjectImportException(o, message);
                }
            }            
        }

        public void Compile()
        {
            ConnectToNAV();
            string message;
            if (!ProcessResult(_objectDesigner.CompileObjects("SORTING(Field1,Field2,Field3) WHERE(Field6=1(No))"), out message))
                Log.Warning("Compilation Errors\n{@CompilationErrorMessages}", message);                
        }        

        #region private methods       
        private int ReadObjectsToStream(string strFilter, Stream objStream)
        {
            IStream pOutStm = null;
            CreateStreamOnHGlobal(0, true, out pOutStm);
            int result = this._objectDesigner.ReadObjects(strFilter, pOutStm);
            if (result == 0)
            {
                result = StreamSupport.ToStream(pOutStm, objStream);
            }
            return result;
        }

        private int WriteObjectFromStream(Stream stream)
        {
            IStream source = StreamSupport.ToIStream(stream);
            int result = this._objectDesigner.WriteObjects(source);

            return result;
        }        

        private void ConnectToNAV()
        {
            int intFound = 0;
            {
                IDictionary<string, object> runningObjects = GetActiveObjectList(DefaultMonikerName);
                if (runningObjects != null)
                {
                    foreach (string strKey in runningObjects.Keys)
                    {
                        string progId = strKey;
                        if (progId.IndexOf("{") != -1)
                        {
                            // Convert a class id into a friendly prog Id
                            progId = ConvertClassIdToProgId(progId);
                        }
                        IObjectDesigner getObj = GetActiveObject(progId) as IObjectDesigner;
                        if (getObj != null && MatchingConnection(getObj))
                        {
                            this._objectDesigner = getObj as IObjectDesigner;
                            intFound++;
                            break;
                        }
                    }
                }
            }
            if (_objectDesigner == null)
                throw new NavTfsException(string.Format("Could not connect to the Microsoft Dynamics NAV Object Designer. Ensure a Microsoft Dynamics NAV Classic Client is runnig"));            
        }

        private bool MatchingConnection(IObjectDesigner _objectDesigner)
        {
            string databaseName = null;
            int iResult = _objectDesigner.GetDatabaseName(out databaseName);
            return iResult == 0 && String.Compare(mConnection.GetDatabaseName(), databaseName) == 0;
        }

        private static string ConvertProgIdToClassId(string progID)
        {
            Guid testGuid;
            try
            {

                CLSIDFromProgIDEx(progID, out testGuid);
            }
            catch
            {
                try
                {
                    CLSIDFromProgID(progID, out testGuid);
                }
                catch
                {
                    return progID;
                }
            }
            return testGuid.ToString().ToUpper();
        }

        private static string ConvertClassIdToProgId(string classID)
        {
            Guid testGuid = new Guid(classID.Replace("!", ""));
            string progId = null;
            try
            {
                ProgIDFromCLSID(ref testGuid, out progId);
            }
            catch (Exception)
            {
                return null;
            }
            return progId;
        }

        private static object GetActiveObject(string progId)
        {
            // Convert the prog id into a class id
            string classId = progId;

            IntPtr numFetched = IntPtr.Zero;
            IRunningObjectTable pRot = null;
            IEnumMoniker pMonkEnum = null;
            IMoniker[] monikers = new IMoniker[1];
            IBindCtx pCtx;
            string displayName;

            try
            {
                // Open the running objects table.
                GetRunningObjectTable(0, out pRot);
                pRot.EnumRunning(out pMonkEnum);
                pMonkEnum.Reset();

                // Iterate through the results
                while (pMonkEnum.Next(1, monikers, numFetched) == 0)
                {
                    CreateBindCtx(0, out pCtx);
                    monikers[0].GetDisplayName(pCtx, null, out displayName);
                    Marshal.ReleaseComObject(pCtx);

                    if (classId == null || displayName.IndexOf(classId) != -1)
                    {
                        // Return the matching object
                        object objReturnObject;
                        pRot.GetObject(monikers[0], out objReturnObject);
                        return objReturnObject;
                    }
                }
                return null;
            }
            finally
            {
                // Free resources
                if (pRot != null)
                    Marshal.ReleaseComObject(pRot);
                if (pMonkEnum != null)
                    Marshal.ReleaseComObject(pMonkEnum);
            }
        }


        private static IDictionary<string, object> GetActiveObjectList(string filter)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            IntPtr numFetched = IntPtr.Zero;
            IRunningObjectTable pRot = null;
            IEnumMoniker pMonkEnum = null;
            IMoniker[] monikers = new IMoniker[1];
            IBindCtx pCtx;
            string runningObjectName;
            object runningObjectVal;

            try
            {
                GetRunningObjectTable(0, out pRot);
                pRot.EnumRunning(out pMonkEnum);
                pMonkEnum.Reset();
                while (pMonkEnum.Next(1, monikers, numFetched) == 0)
                {                    
                    CreateBindCtx(0, out pCtx);
                    monikers[0].GetDisplayName(pCtx, null, out runningObjectName);
                    Marshal.ReleaseComObject(pCtx);
                    pRot.GetObject(monikers[0], out runningObjectVal);

                    if (runningObjectName.IndexOf(filter) != -1 && runningObjectName.IndexOf("database") != -1)
                    {
                        if (result.ContainsKey(runningObjectName))
                        {
                            throw new InvalidDataException(string.Format("Multiple instances of {0} are found; cannot establish a connection to the Object Designer",runningObjectName));                            
                        }
                        result[runningObjectName] = runningObjectVal;
                    }
                }
            }
            finally
            {
                if (pRot != null)
                    Marshal.ReleaseComObject(pRot);
                if (pMonkEnum != null)
                    Marshal.ReleaseComObject(pMonkEnum);
            }
            return result;
        }


        private bool ProcessResult(int result, out string message)
        {
            message = string.Empty;
            if (result != 0)
            {
                IErrorInfo ppIErrorInfo = null;
                GetErrorInfo(0, out ppIErrorInfo);                
                if (ppIErrorInfo != null)
                {
                    ppIErrorInfo.GetDescription(out message);                    
                }              
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region Native methods and interfaces\
        [DllImport("ole32.dll", PreserveSig = false)]
        private static extern int CLSIDFromProgIDEx([MarshalAs(UnmanagedType.LPWStr)] string progId, out Guid clsid);

        [DllImport("ole32.dll", PreserveSig = false)]
        private static extern int CLSIDFromProgID([MarshalAs(UnmanagedType.LPWStr)] string progId, out Guid clsid);

        [DllImport("oleaut32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetErrorInfo(int dwReserved, [MarshalAs(UnmanagedType.Interface)] out IErrorInfo ppIErrorInfo);

        [DllImport("ole32.dll")]
        private static extern int CreateBindCtx(int reserved, out IBindCtx ppbc);

        [DllImport("OLE32.DLL")]
        private static extern int CreateStreamOnHGlobal(int hGlobalMemHandle, bool fDeleteOnRelease, out IStream pOutStm);

        [DllImport("ole32.dll")]
        private static extern int ProgIDFromCLSID([In()]ref Guid clsid, [MarshalAs(UnmanagedType.LPWStr)]out string lplpszProgID);

        [DllImport("ole32.dll")]
        private static extern int GetRunningObjectTable(int reserved, out IRunningObjectTable prot);

        [ComImport, SuppressUnmanagedCodeSecurity, Guid("1CF2B120-547D-101B-8E65-08002B2BD119"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        private interface IErrorInfo
        {
            [PreserveSig]
            int GetGUID();
            [PreserveSig]
            int GetSource([MarshalAs(UnmanagedType.BStr)] out string pBstrSource);
            [PreserveSig]
            int GetDescription([MarshalAs(UnmanagedType.BStr)] out string pBstrDescription);
        }
        #endregion
        

        public void Export(IWorkspaceObject obj)
        {
            Export(string.Format("SORTING(Field1,Field2,Field3) WHERE(Field1=1({0}),Field3=1({1}))", (int)obj.Type, obj.ID),obj.LocalSpec);
        }


        public void Design(IWorkspaceObject obj)
        {
            throw new NotImplementedException();
        }
    }
}
