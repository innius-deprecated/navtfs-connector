﻿using System;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;

namespace TI.NavTfsConnector.NavObjectDesigner
{
    internal static class StreamSupport
    {
        internal static unsafe IStream ToIStream(Stream stream)
        {
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            uint num = 0;
            IntPtr pcbWritten = new IntPtr((void*)&num);
            IStream pOutStm = null;
            CreateStreamOnHGlobal(0, true, out pOutStm);
            pOutStm.Write(buffer, buffer.Length, pcbWritten);
            pOutStm.Seek((long)0, 0, IntPtr.Zero);

            return pOutStm;
        }

        internal static unsafe MemoryStream ToMemoryStream(IStream comStream)
        {
            MemoryStream stream = new MemoryStream();
            byte[] pv = new byte[100];
            uint num = 0;
            IntPtr pcbRead = new IntPtr((void*)&num);
            comStream.Seek((long)0, 0, IntPtr.Zero);
            do
            {
                num = 0;
                comStream.Read(pv, pv.Length, pcbRead);
                stream.Write(pv, 0, (int)num);
            }
            while (num > 0);
            return stream;
        }

        internal static unsafe int ToStream(IStream comStream, Stream stream)
        {
            byte[] pv = new byte[100];
            uint num = 0;
            IntPtr pcbRead = new IntPtr((void*)&num);
            comStream.Seek((long)0, 0, IntPtr.Zero);
            do
            {
                num = 0;
                comStream.Read(pv, pv.Length, pcbRead);
                stream.Write(pv, 0, (int)num);
            }
            while (num > 0);

            return 0;
        }


        #region Native methods
        [DllImport("ole32.dll")]
        public static extern void CreateBindCtx(int reserved, out IBindCtx ppbc);
        [DllImport("OLE32.DLL")]
        public static extern int CreateStreamOnHGlobal(int hGlobalMemHandle, bool fDeleteOnRelease, out IStream pOutStm);
        #endregion

    }
}

