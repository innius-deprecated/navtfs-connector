﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace TI.NavTfsConnector.NavObjectDesigner
{
    public interface INavObjectDesigner
    {
        void ExportLockedObjects(string fileName);
        void Export(string filter, string fileName);
        void Export(IWorkspaceObject obj);
        void Import(IEnumerable<IWorkspaceObject> objects);
        void Compile();
        void Design(IWorkspaceObject obj);
    }
}
