﻿
namespace TI.NavTfsConnector.NavObjectDesigner
{
    public class Nav2013R2 : Nav2013, INavObjectDesigner
    {
        public Nav2013R2(IConnection sccConnection) : base (sccConnection){}

        protected override string ImportCommand(string fileName)
        {
            return base.ImportCommand(fileName) + AddValidateTableChangesPart(); 
        }

        protected override string CompileCommand(string filter)
        {
            return base.CompileCommand(filter) + AddValidateTableChangesPart();
        }

        private string AddValidateTableChangesPart()
        {
            return ",validatetablechanges=0";
        }
    }

    
}
