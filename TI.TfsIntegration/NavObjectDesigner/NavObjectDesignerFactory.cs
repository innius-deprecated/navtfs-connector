﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector.NavObjectDesigner
{
    public static class NavObjectDesignerFactory
    {
        public static INavObjectDesigner GetObjectDesigner(IConnection connection)
        {
            INavObjectDesigner objectDesiger;
            var result = GetNavVersion(connection.NavClientFolder);
            if (result.Item1 < 7) 
            {                
                objectDesiger = new Nav2009(connection); 
            }
            else if ((result.Item1 == 7) && (result.Item2 == 0))
            {
                objectDesiger = new Nav2013(connection);                
            }            
            else if ((result.Item1 == 7) && (result.Item2 > 0))
            {
                objectDesiger = new Nav2013R2(connection);             
            }
            else
            {
                objectDesiger = new Nav2015(connection);
            }
            Log.Verbose("Object Designer Type {@version}", objectDesiger.GetType().Name);            
            return objectDesiger;            
        }

        static private Tuple<int, int> GetNavVersion(string path)
        {
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Path.Combine(path,"finsql.exe"));
            Log.Verbose("The version of the finsql executable in path {@path} is {@version}", path, fvi.FileVersion);
            return new Tuple<int, int>(fvi.FileMajorPart, fvi.FileMinorPart);
        }
    }
}
