﻿
namespace TI.NavTfsConnector.NavObjectDesigner
{
    public class Nav2015 : Nav2013, INavObjectDesigner
    {
        public Nav2015(IConnection sccConnection) : base (sccConnection){}

        protected override string ImportCommand(string fileName)
        {
            return base.ImportCommand(fileName) + AddVersionSpecificOptions(); 
        }

        protected override string CompileCommand(string filter)
        {
            return base.CompileCommand(filter) + AddVersionSpecificOptions();
        }

        private string AddVersionSpecificOptions()
        {
            return ",synchronizeschemachanges=force";
        }
    }

    
}
