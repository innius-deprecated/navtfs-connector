﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TI.NavTfsConnector.NavObjectDesigner;
using System.Collections;
using Serilog;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace TI.NavTfsConnector.NavObjectDesigner
{
    public class Nav2013 : INavObjectDesigner
    {
        private string mDevEnvPath;
        private string mServerName;
        private string mDatabaseName;
        private IConnection mConnection;

        public Nav2013(IConnection tfsConnection)
        {
            mConnection = (IConnection)tfsConnection;
            this.mDevEnvPath = System.IO.Path.Combine(mConnection.NavClientFolder, "finsql.exe");
            SqlConnection connection = new SqlConnection(mConnection.ConnectionString);
            this.mServerName = connection.DataSource;
            this.mDatabaseName = connection.Database;
        }

        public void ExportLockedObjects(string fileName)
        {
            Export("Locked=1", fileName);
        }

        public void Export(string filter, string fileName)
        {
            ExecuteExport(filter, fileName);
        }

        public void Import(IEnumerable<IWorkspaceObject> objects)
        {
            if (objects.Count() > 0)
            {
                var queue = Enqueue(objects);

                Action t1 = () =>
                {
                    IWorkspaceObject obj;
                    while (queue.TryDequeue(out obj))
                    {
                        Log.Verbose("Import {@Item} in Nav", obj.ItemName);
                        ImportObject(obj);
                    }
                };

                try
                {
                    Parallel.Invoke(t1, t1, t1, t1);
                }
                catch (AggregateException e)
                {
                   throw e.InnerExceptions[0];                
                }
            }           
        }

        private ConcurrentQueue<IWorkspaceObject> Enqueue(IEnumerable<IWorkspaceObject> objects)
        {
            ConcurrentQueue<IWorkspaceObject> queue = new ConcurrentQueue<IWorkspaceObject>();        

            foreach(var o in objects)
            {
                queue.Enqueue(o);
            }
            
            return queue;
        }

        public void Compile()
        {
            // compile all objects which are not yet compiled 
            CompileObjects(string.Format("Compiled={0}", 0));
        }

        public void Design(IWorkspaceObject obj)
        {
            Execute2(string.Format("servername={0},database={1}, designobject={2} {3}", mServerName, mDatabaseName, obj.Type, obj.ID), false);
        }

        #region private methods
        private string ExecuteExport(string filter, string fileName)
        {
            Execute(string.Format("command=exportobjects, file=\"{0}\",servername={1},database={2},filter={3}", fileName, mServerName, mDatabaseName, filter));
            return fileName;
        }

        protected virtual string ImportCommand(string fileName)
        {
            return string.Format("command=importobjects, file=\"{0}\",servername={1},database={2}", fileName, mServerName, mDatabaseName);
        }

        private string ExecuteImport(string fileName)
        {
            return Execute2(ImportCommand(fileName));
        }

        protected virtual string CompileCommand(string filter)
        {
            return string.Format("command=compileobjects, servername={0},database={1},filter={2}", mServerName, mDatabaseName, filter);
        }

        private string ExecuteCompile(string filter)
        {
            return Execute2(CompileCommand(filter));
        }

        private bool IsValidateTableChangeSupported()
        {
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(this.mDevEnvPath);
            if (fileVersionInfo.FileMajorPart == 7 & fileVersionInfo.FileMinorPart == 0)
                return false;
            return true;
        }

        private void ImportObject(IWorkspaceObject obj)
        {
            string result = ExecuteImport(obj.LocalSpec);
            List<IWorkspaceObject> failedObjects = new List<IWorkspaceObject>();
            if (!String.IsNullOrEmpty(result) && (IsNavErrorMessage(result)))                
                throw new NavTfsObjectImportException(obj, string.Format("{0} has import errors:\n{1}",obj.ItemName,result));
        }

        private void CompileObjects(string filter)
        {
            string result = ExecuteCompile(filter);
            List<WorkspaceObject> failedObjects = new List<WorkspaceObject>();
            if (!String.IsNullOrEmpty(result))
            {
                foreach (Match m in Regex.Matches(result, @"(?<errortext>^\[\d*\].*(.|\n){0,}?)-- Object: (?<type>\w*)\s(?<id>\d*)\s(?<name>.*)", RegexOptions.Multiline))
                {
                    WorkspaceObject failed = new WorkspaceObject { Type = (NavObjectType)Enum.Parse(typeof(NavObjectType), m.Groups["type"].Value, true), ID = int.Parse(m.Groups["id"].Value), Connection = mConnection };
                    Log.Verbose("Item {@item} has compilation errors {@compilationerrors}", failed.ItemName, m.Groups["errortext"].Value);
                }
            }
        }

        private void Execute(string arguments)
        {
            string message = Execute2(arguments);
            if (!string.IsNullOrEmpty(message) && (IsNavErrorMessage(message)))
            {                                                
                Log.Error("An error {@message} occured while executing the finsql command with arguments {@arguments}", message, arguments);
                throw new NavTfsException(message);                
            }
        }

        private bool IsNavErrorMessage(string message)
        {
            Log.Warning("The Microsoft Dynamics NAV development environment returned {@warning}", message);
            // License warning which should be ignored
            if (message.StartsWith("[18023763]"))
                return false;

            return true;
        }

        private string Execute2(string arguments)
        {
            return Execute2(arguments, true);
        }

        private string Execute2(string arguments, bool wait)
        {
            string logFile = GetTempFileName(mConnection);

            arguments += String.Format(",logfile=\"{0}\"", logFile);

            if (!File.Exists(mDevEnvPath))
                throw new FileNotFoundException(string.Format("The path for the Micorosoft Dynamics NAV Object Designer does not exist [{0}]", mDevEnvPath));

            System.Diagnostics.ProcessStartInfo pInfo = new ProcessStartInfo(this.mDevEnvPath, arguments);

            pInfo.WindowStyle = ProcessWindowStyle.Hidden;
            pInfo.CreateNoWindow = true;

            Process process = Process.Start(pInfo);
            string message = string.Empty;
            if (wait)
            {
                process.WaitForExit();
                if (process.ExitCode == 1)
                {
                    if (File.Exists(logFile))
                    {
                        message = File.ReadAllText(logFile);
                    } 
                    else 
                    {
                        Log.Error("Unknown error occured in finsql while processing {arguments}", arguments);
                    }                    
                }
            }
            File.Delete(logFile);
            return message;
        }

        private static string GetTempFileName(IConnection connection)
        {
            string tempPath = Path.Combine(System.IO.Path.GetTempPath(), connection.Name, System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }
            return Path.Combine(tempPath, Guid.NewGuid().ToString() + ".txt");
        }


        public void Export(IWorkspaceObject obj)
        {
            Export(string.Format("Type={0};ID={1}", obj.Type, obj.ID), obj.LocalSpec);
        }
    }

        #endregion
}
