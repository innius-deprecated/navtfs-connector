﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public interface IConnectionRepository
    {
        IConnection Insert(IConnection connection);
        IConnection Update(IConnection connection);
        IConnection Delete(IConnection connection);
        IEnumerable<IConnection>Load();
        string GetConnectionStoreFileName();
    }
}
