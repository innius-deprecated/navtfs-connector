﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TI.NavTfsConnector;

namespace TI.NavTfsConnector
{
    public enum NavClientVersionType
    {
        Classic = 61,
        Nav2013 = 70,
        Nav2013R2 = 71
    }
    
    public enum NamingConvention
    {
        Default = 1,
        Classic = 2,
        Custom = 3
    }

    public enum ConnectionStatus
    {
        Ready = 0,
        Processing = 1,
        Stopped = 2,
        Error = 3,
    }

    public interface IConnection
    {
        string Name { get; set; }
        
        Uri ProjectCollection { get; set; }
        
        string WorkspaceFolder { get; set; }
        string ConnectionString { get; set; }
        string NavClientFolder { get; set; }

        

        string Database { get; set; }

        string LastErrorMessage { get; set; }

        NamingConvention ObjectNamingConvention { get; set; }
        string NamingExpression { get; set; }

        Hashtable Cache { get; set; }

        ConnectionStatus Status { get; set; }

        void AddToHistory(IWorkspaceObject obj);
    }
}
