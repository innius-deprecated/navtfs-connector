﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public interface INavObjectAdapter
    {
        IEnumerable<IWorkspaceObject> Load();
        void Save(IEnumerable<IWorkspaceObject> navObjects);
        void EnableAdapter();
    }
}
