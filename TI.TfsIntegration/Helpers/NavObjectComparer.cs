﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    internal class NavObjectComparer : IEqualityComparer<IWorkspaceObject>
    {
        int IEqualityComparer<IWorkspaceObject>.GetHashCode(IWorkspaceObject navObj)
        {           
            if (navObj.Type == 0)
            {
                return 0;
            }
            return navObj.ID.GetHashCode();
        }

        bool IEqualityComparer<IWorkspaceObject>.Equals(IWorkspaceObject x1, IWorkspaceObject x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) ||
                object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return (x1.ID == x2.ID & (x1.Type == x2.Type));
        }
    }
}
