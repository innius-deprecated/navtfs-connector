﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO.IsolatedStorage;
using System.Security.Principal;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace TI.NavTfsConnector
{
    static public class Utilities
    {
        public const int DYNAMICS_NAV_ENCODING = 437;

        public static string GetUserDomainName()
        {
            return string.IsNullOrEmpty(Environment.UserDomainName) ? Environment.UserName : string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName);
        }
    
        public static string GetTempFileName()
        {
            return System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".txt";
        }

        public static IEnumerable<Tuple<IWorkspaceObject, string>> GetObjectsFromExportFile(IConnection connection, string fileName)
        {
            string expr = @"^OBJECT.*\n\{(.*\n){1,}?\}";
            if (File.Exists(fileName))
            {
                foreach (Match m in Regex.Matches(File.ReadAllText(fileName,Encoding.GetEncoding(DYNAMICS_NAV_ENCODING)), expr, RegexOptions.Multiline))
                {
                    yield return ParseObjectTextFile(connection, m.Value);
                }
            }
        }

        private static Tuple<IWorkspaceObject, string> ParseObjectTextFile(IConnection connection, string source)
        {
            GroupCollection properties = Regex.Match(source, @"^OBJECT (?<type>\w*)\s(?<id>\d*)\s(?<name>.*\w)").Groups;
            WorkspaceObject obj = new WorkspaceObject { Type = (NavObjectType)Enum.Parse(typeof(NavObjectType), properties["type"].Value, true), ID = int.Parse(properties["id"].Value), Name = properties["name"].Value };
            obj.Connection = connection;
            return new Tuple<IWorkspaceObject, string>(obj, source);
        }        
    }
}
