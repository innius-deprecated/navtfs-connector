﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TI.NavTfsConnector;

namespace TI.NavTfsConnector
{
    public abstract class BaseConnection : IConnection
    {
        public virtual string Name { get; set; }

        public virtual Uri ProjectCollection { get; set; }

        public virtual string WorkspaceFolder { get; set; }
        
        private string mConnectionString;
        public virtual string ConnectionString
        {
            get
            { 
                return mConnectionString; 
            }
            set
            {                
                mConnectionString = value;
                var sb = new SqlConnectionStringBuilder();
                sb.ConnectionString = value;
                this.Database = sb.InitialCatalog;                
            }
        }
        public virtual string NavClientFolder { get; set; }
                
        public virtual string Database { get; set; }
        
        public virtual NamingConvention ObjectNamingConvention { get; set; }
        public virtual string NamingExpression { get; set; }

        private Hashtable _Cache = new Hashtable();
        public Hashtable Cache 
        {
            get
            { 
                return _Cache; 
            }
            set
            { 
                _Cache = value == null ? new Hashtable() : value; 
            }
        }
    
        public virtual ConnectionStatus Status { get; set; }
        public virtual string LastErrorMessage {get;set;}
        public abstract void AddToHistory(IWorkspaceObject obj);
    }
}
