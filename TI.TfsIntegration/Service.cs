﻿using Serilog;
using Serilog.Context;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TI.NavTfsConnector.NavObjectDesigner;

namespace TI.NavTfsConnector
{
    public class Service
    {
        public event EventHandler OnErrorEventHandler;

        private Thread mWorker;
        private IEnumerable<IConnection> mConnections;

        public void Start(IEnumerable<IConnection> connections)
        {       
            mConnections = connections;
            mWorker = new Thread(Synchronize);
            mWorker.IsBackground = true;
            mWorker.Name = "syncthread";
            mWorker.Start();
            Log.Information("Synchronization service has been started succesfully");
        }

        public void Stop()
        {
            if (mWorker != null)
                mWorker.Abort();
        }

        private void Synchronize()
        {
            while (true)
            {
                foreach (var c in from cn in mConnections where cn.Status == ConnectionStatus.Ready select cn)
                {
                    SynchronizeConnection(c);
                }
                Thread.Sleep(int.Parse(ConfigurationManager.AppSettings["SynchronizationInterval"]));                
            }
        }

        private void SynchronizeConnection(IConnection connection)
        {
            using (LogContext.PushProperty("Connection", connection.Name))
            {
                try
                {
                    Log.Verbose("Processing Started");

                    connection.Status = ConnectionStatus.Processing;

                    Configuration config = new Configuration();
                    config.Register(connection);
                    var connector = config.Container.GetInstance<Connector>();
                    connector.Synchronize();                                        
                    if (connection.Status == ConnectionStatus.Processing)
                        connection.Status = ConnectionStatus.Ready;
                    
                    Log.Verbose("Processing Completed");
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error while processing the connection");
                    connection.Status = ConnectionStatus.Error;
                    connection.LastErrorMessage = ex.Message;
                    // new event
                    OnError(new OnErrorEventArgs(connection));
                }
            }
        }

        #region events
        
        public class OnErrorEventArgs: EventArgs
        {
            public readonly IConnection _connection;

            internal OnErrorEventArgs(IConnection connection)
            {
                _connection = connection;
            }
        }

        protected virtual void OnError(OnErrorEventArgs e)
        {
            if (OnErrorEventHandler != null) OnErrorEventHandler(this, e);
        }

       
        #endregion
    }



    public class Configuration
    {
        private Container mContainer;

        public Container Container { get { return mContainer; } }

        public void Register(IConnection connection)
        {
            mContainer = new Container();

            mContainer.RegisterSingle(connection);
            mContainer.Register<INavObjectDesigner>(()=> NavObjectDesignerFactory.GetObjectDesigner(connection),Lifestyle.Singleton);
            mContainer.Register<IConnector, Connector>();
            mContainer.Register<IVersionControlSystem, TfsVersionControl>();
            mContainer.Register<INavObjectAdapter, NavObjectSqlAdapter>();
            mContainer.Register<IVersionControlClient, VersionControlClient>();
        }
    }

}
