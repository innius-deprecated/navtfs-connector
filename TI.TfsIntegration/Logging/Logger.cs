﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector.Logging
{
    static public class LogHelper
    {
        public static ILogger ConfigureLogger(string applicationName)
        {
            Log.Logger = new LoggerConfiguration()
                 .ReadAppSettings()
                 .Enrich.FromLogContext()
                 .Enrich.WithMachineName()
                 .Enrich.WithProperty("UserName", Environment.UserName)
                 .Enrich.WithProperty("ApplicationName", applicationName)
                 .Enrich.With(new LicenseContextEnricher())
                 .CreateLogger();

            return Log.Logger;
        }
    }

    public class LicenseContextEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {                        
            logEvent.AddPropertyIfAbsent(
                propertyFactory.CreateProperty("LicenseNumber", LicenseContext.MyLicense.LicenseNumber));

            logEvent.AddPropertyIfAbsent(
                propertyFactory.CreateProperty("LicensedTo", LicenseContext.MyLicense.LicensedTo));

            logEvent.AddPropertyIfAbsent(
                propertyFactory.CreateProperty("LicenseName", LicenseContext.MyLicense.Name));

            logEvent.AddPropertyIfAbsent(
                propertyFactory.CreateProperty("LicenseEmail", LicenseContext.MyLicense.Email));            
        }
    }
}
