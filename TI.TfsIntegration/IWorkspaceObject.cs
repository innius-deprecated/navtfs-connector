﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public interface IWorkspaceObject
    {
        NavObjectType Type { get; set; }
        int ID { get; set; }
        string Name { get; set; }
        bool Locked { get; set; }
        DateTime Date { get; set; }
        Int64 Timestamp { get; set; }
        int VersionLocal { get; set; }
        string LockedBy { get; set; }
        ChangeType ChangeType { get; set; }        
        IConnection Connection { get; set; }
        SyncAction SyncAction { get; set; }
        string ItemName {get;}
        string LocalSpec {get;}
        
        void Verbose(string messageTemplate, params object[] propertyValues);
        
        void Lock();        
        void Unlock();
    }
}
