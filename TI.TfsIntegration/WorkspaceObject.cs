﻿using Serilog;
using Serilog.Context;
using StringFormat;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public enum NavObjectType
    {
        Codeunit = 5,
        Dataport = 4,
        Form = 2,
        MenuSuite = 7,
        Report = 3,
        Table = 1,
        XmlPort = 6,
        Page = 8,
        Query = 9
    }

    public enum ChangeType
    {
        // Summary:
        //     No change.
        None = 1,
        //
        // Summary:
        //     Add change type.
        Add = 2,
        //
        // Summary:
        //     Edit change type.
        Edit = 4,
        //
        // Summary:
        //     Encoding change type.
        Encoding = 8,
        //
        // Summary:
        //     Rename change type.
        Rename = 16,
        //
        // Summary:
        //     Delete change type.
        Delete = 32,
        //
        // Summary:
        //     Undelete change type.
        Undelete = 64,
        //
        // Summary:
        //     Branch change type.
        Branch = 128,
        //
        // Summary:
        //     Merge change type.
        Merge = 256,
        //
        // Summary:
        //     Lock change type.
        Lock = 512,
        //
        // Summary:
        //     Rollback change type.
        Rollback = 1024,
        //
        // Summary:
        //     Source rename change type.
        SourceRename = 2048,
        //
        Property = 8192,
    }
      
    [Flags]
    public enum SyncAction
    {
        None = 0,
        Add = 1,
        Checkout = 2,
        Unlock = 4,
        Lock =8 ,
        Import =16,
        Export = 32,
        Update = 64
    }

    public class WorkspaceObject : IWorkspaceObject
    {
        private static char[] chars = "0123456789".ToCharArray();
        private const string CLASSIC_FORMAT = "{TYPE:3}{ID}.txt";
        private const string DEFAULT_FORMAT = "{Type}{ID}.txt";
        private enum TypeFormat
        {
            camelcase = 0,
            lowercase = 1, 
            uppercase = 2,             
        }

        public WorkspaceObject(IConnection connection, string fileName) : this()
        {
            this.Connection = connection;
            var p = ParseFileName(fileName);
            this.Type = p.Item1;
            this.ID = p.Item2;            
        }

        public WorkspaceObject()
        {           
            this.LockedBy = "";
            this.Locked = false;
            this.VersionLocal = 0;
            this.ChangeType = ChangeType.None;
        }

        #region properties
        public NavObjectType Type { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Locked { get; set; }
        public DateTime Date { get; set; }
        public Int64 Timestamp { get; set; }
        public int VersionLocal { get; set; }
        public string LockedBy { get; set; }
        public ChangeType ChangeType { get; set; }
        public IConnection Connection { get; set; }
        public SyncAction SyncAction { get; set; }        

        public string ItemName
        {
            get
            {
                return FormatFileName(this.Type, this.ID, this.Connection.ObjectNamingConvention, this.Connection.NamingExpression);
            }
        }

        public string LocalSpec
        {
            get
            {
                if (this.Connection.WorkspaceFolder != null)
                    return Path.Combine(this.Connection.WorkspaceFolder, ItemName);
                return ItemName;
            }
        }

        #endregion        
       
        public void Verbose(string messageTemplate, params object[] propertyValues)
        {
            using (LogContext.PushProperty("Object", this))
                Log.Verbose(messageTemplate, propertyValues);
        }

        public void Lock()
        {
            this.Locked = true;
            this.LockedBy = string.IsNullOrEmpty(Environment.UserDomainName) ? Environment.UserName : string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName);
        }

        public void Unlock()
        {
            this.ChangeType = ChangeType.None;
            this.Locked = false;
            this.LockedBy = "";
        }
        
        public override bool Equals(object obj)
        {
            WorkspaceObject obj2 = obj as WorkspaceObject;
            if (obj2 == null)
                return false;
            return (
                this.Type == obj2.Type && this.ID == obj2.ID && this.Locked == obj2.Locked && this.LockedBy == obj2.LockedBy && this.VersionLocal == obj2.VersionLocal && this.ChangeType == obj2.ChangeType);
        }

        public override int GetHashCode()
        {
            return (string.Format("{0}{1}", this.Type, this.ID).GetHashCode());
        }

        public override string ToString()
        {
            return string.Format("{0}{1}", this.Type, this.ID);
        }

        #region private methods
        private string FormatFileName(NavObjectType type, int id, NamingConvention format, string expression)
        {
            switch (format)
            {
                case NamingConvention.Classic:
                    return FormatFileName(type, id, CLASSIC_FORMAT);
                case NamingConvention.Custom:
                    return FormatFileName(type, id, expression);                    
                default:
                    return FormatFileName(type, id, DEFAULT_FORMAT);
            }
        }

        private string FormatFileName(NavObjectType type, int id, string expression)
        {
            var format = ParseFormatExpression(expression);
            string typeStr = Enum.GetName(typeof(NavObjectType), type);
            
            switch (format["type"].Item1)
            {
                case "type":
                    typeStr = typeStr.ToLower();
                    break;
                case "TYPE":
                    typeStr = typeStr.ToUpper();
                    break;
            }
            if (format["type"].Item2 > 0)
                typeStr = typeStr.Substring(0, format["type"].Item2);

            
            return (TokenStringFormat.Format(expression.ToLower(), new { type = typeStr, id = id.ToString().PadLeft(format["id"].Item2,'0')}));            
        }

        private Tuple<NavObjectType, int> ParseFileName(string fileName)
        {
            try
            {
                switch (this.Connection.ObjectNamingConvention)
                {
                    case NamingConvention.Classic:
                        return ParseObjectFileName(fileName, CLASSIC_FORMAT);
                    case NamingConvention.Custom:
                        return ParseObjectFileName(fileName, Connection.NamingExpression);
                    default:
                        return ParseObjectFileName(fileName, string.Empty);
                }
            }
            catch (FormatException)
            {
                throw new NavTfsException("The name of file {0} does not have the expected format [Object Type][Object ID].txt");
            }
        }

        private Tuple<NavObjectType, int> ParseObjectFileName(string fileName, string format)
        {
            string itemName = Path.GetFileNameWithoutExtension(fileName);
            string typeStr = new string(Array.FindAll<char>(itemName.ToCharArray(), (c => (char.IsLetter(c)))));
            string objectId = new string(Array.FindAll<char>(itemName.ToCharArray(), (c => (char.IsNumber(c)))));
            
            NavObjectType type;

            if (!TryParseObjectType(typeStr, format, out type))
                throw new InvalidCastException(string.Format("The file name {0} cannot be converted to a Dynamics NAV Object Type", fileName));

            return new Tuple<NavObjectType, int>(type, int.Parse(objectId));
        }

        private string GetLetters(string str)
        {
            char[] arr = str.ToCharArray();

            arr = Array.FindAll<char>(arr, (c => (char.IsLetter(c))));
                                                                                            
            return new string(arr);
        }

        private bool TryParseObjectType(string typeStr, string format, out NavObjectType type)
        {
            if (Enum.TryParse<NavObjectType>(typeStr, out type))
                return true;

            var f = ParseFormatExpression(format);
            var len = f["type"].Item2;
            foreach (int value in Enum.GetValues(typeof(NavObjectType)))
            {
                var enumStr = Enum.GetName(typeof(NavObjectType), value);
                if (string.Compare(enumStr.Substring(0, len > 0 ? len : enumStr.Length), typeStr, true) == 0)
                {
                    type = (NavObjectType)value;
                    return true;
                }
            }
            return false;
        }

        private Dictionary<string, Tuple<String, int>>ParseFormatExpression(string formatExpression)
        {
            string strRegex = @"(\{(?<part>\w*)(:(?<len>\d{1,}?)){0,1}\})";
            Regex myRegex = new Regex(strRegex, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            
            var result = myRegex.Matches(formatExpression);

            var dict = new Dictionary<string, Tuple<String, int>>();
            foreach (Match r in result)
            {
                int length;
                int.TryParse(r.Groups["len"].Value, out length);               
                dict[r.Groups["part"].Value.ToLower()] = new Tuple<string,int>(r.Groups["part"].Value, length);
            }
            return dict;
        }

     
        #endregion
    }
}
