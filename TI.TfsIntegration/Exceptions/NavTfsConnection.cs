﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public class NavTfsException : Exception
    {
        public NavTfsException(string message):base(message)
        {}
    }

    public class NavTfsObjectImportException : Exception
    {
        public NavTfsObjectImportException(IWorkspaceObject obj, string message)
            : base(message)
        {
            this.WorkspaceObject = obj;
        }

        public IWorkspaceObject WorkspaceObject { get; private set; }
    }
}
