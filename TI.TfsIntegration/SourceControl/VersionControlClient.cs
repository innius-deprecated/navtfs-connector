﻿using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public class VersionControlClient : IVersionControlClient
    {
        private IVersionControlSystem _versionControlSystem;
        private IConnection _connection;

        public VersionControlClient(IConnection connection, IVersionControlSystem versionControlSystem)
        {
            _versionControlSystem = versionControlSystem;
            _connection = connection;
        }

        public void Checkout(IWorkspaceObject navObject)
        {
            _versionControlSystem.TfsCheckout(navObject);
        }

        public IEnumerable<IWorkspaceObject> GetItems()
        {
            var objects = from f in _versionControlSystem.GetItems()
                          select GetNewWorkspaceObject(f);
            
            Log.Verbose("{@NumberOfItems} found in {@WorkFolder}", objects.Count(), _connection.WorkspaceFolder);            

            return objects;
        }     

        public void Add(IWorkspaceObject navObject)
        {
            _versionControlSystem.TfsAdd(navObject);
        }

        public void Verify()
        {
            _versionControlSystem.VerifyProjectCollection();
        }

        private IWorkspaceObject GetNewWorkspaceObject(Tuple<String, int, ChangeType> f)
        {            
            return new WorkspaceObject(_connection, f.Item1)
            {                
                VersionLocal = f.Item2,
                ChangeType = f.Item3,
            };

        }
                
    }
}
