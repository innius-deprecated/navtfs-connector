﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public interface IVersionControlClient
    {
        void Checkout(IWorkspaceObject navObject);
        IEnumerable<IWorkspaceObject> GetItems();
        void Add(IWorkspaceObject navObject);
    }
}
