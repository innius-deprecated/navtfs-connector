﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public interface IVersionControlSystem
    {
        void TfsCheckout(IWorkspaceObject navObject);
        IEnumerable<Tuple<String, int, ChangeType>> GetItems();
        void TfsAdd(IWorkspaceObject navObject);
        void VerifyProjectCollection();
        void VerifyWorkspace();
    }
}
