﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Framework.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.VersionControl.Common;
using System.Diagnostics;
using Serilog;

namespace TI.NavTfsConnector
{
    public class TfsVersionControl : IVersionControlSystem
    {
        private Workspace mWorkspace;
        private IConnection mConnection;
        private VersionControlServer _versionControlServer = null;

        #region internal Methods
        public TfsVersionControl(IConnection tfsConnection)
        {
            mConnection = (IConnection)tfsConnection;
        }

        public void TfsCheckout(IWorkspaceObject navObject)
        {
            var results = GetWorkspace().PendEdit(navObject.LocalSpec);
            Log.Verbose("{@Item} has been Checkedout", navObject.ItemName);            
        }

        public void TfsAdd(IWorkspaceObject navObject)
        {
            var results = GetWorkspace().PendAdd(navObject.LocalSpec);
            Log.Verbose("{@Item} has been added", navObject.ItemName);            
        }

        public IEnumerable<Tuple<String, int, ChangeType>> GetItems()
        {
            ItemSpec[] itemSpec = { new ItemSpec(mConnection.WorkspaceFolder, RecursionType.Full) };

            return from f in GetWorkspace().GetExtendedItems(itemSpec, DeletedState.NonDeleted, ItemType.File).First<ExtendedItem[]>()
                   select new Tuple<String, int, ChangeType>(!string.IsNullOrEmpty(f.LocalItem) ? f.LocalItem : f.SourceServerItem, f.VersionLocal, (ChangeType)f.ChangeType);
        }

        public void VerifyProjectCollection()
        {
            GetVersionControlServer();
        }

        public void  VerifyWorkspace()
        {
            GetWorkspace();
        }

        #endregion

        #region Private Methods
       
        private static ItemSpec[] GetItemSpecForNavObject(WorkspaceObject navObject)
        {
            ItemSpec[] selectedItemSpec = { new ItemSpec(navObject.LocalSpec, RecursionType.Full) };
            return selectedItemSpec;
        }

        private Workspace GetWorkspace()
        {
            GetVersionControlServer();
            if (mWorkspace == null)
            {
                Log.Verbose("Connect to the workspace of for connection {@Connection}", mConnection);                
                mWorkspace = _versionControlServer.GetWorkspace(mConnection.WorkspaceFolder);
                Log.Verbose("Workspace {@Workspace} is connected to connection {@Connection}",mWorkspace.Name, mConnection);                
            }
            return mWorkspace;
        }

        private void GetVersionControlServer()
        {
            if (_versionControlServer == null)
            {
                Log.Verbose("Get the Team Project Collection for connection {@Connection}", mConnection);
                TfsTeamProjectCollection collection = new TfsTeamProjectCollection(mConnection.ProjectCollection);
                Log.Verbose("Get the Version Control Server for connection {@Connection}", mConnection);
                _versionControlServer = (VersionControlServer)collection.GetService(typeof(VersionControlServer));                
            }
        }

        #endregion      
    }
   
}
