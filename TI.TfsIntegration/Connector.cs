﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.TeamFoundation.VersionControl.Client;
using System.Threading;
using TI.NavTfsConnector.NavObjectDesigner;
using System.Text.RegularExpressions;
using System.Security.Permissions;
using System.Collections;
using System.Resources;
using Serilog;
using Serilog.Context;

namespace TI.NavTfsConnector
{
    public class Connector : IConnector
    {
        private IConnection mConnection;
        private List<WorkspaceObject> mObjectCache = new List<WorkspaceObject>();
        private INavObjectDesigner mNavObjectDesigner;
        private IVersionControlClient mVCCClient;
        private INavObjectAdapter mObjectAdapter;

        public Connector(IConnection tfsConnection, INavObjectDesigner objectDesigner, IVersionControlClient vccClient, INavObjectAdapter objectAdapter)
        {
            mConnection = tfsConnection;
            mNavObjectDesigner = objectDesigner;
            mVCCClient = vccClient;
            mObjectAdapter = objectAdapter;
        }

        public void Synchronize()
        {            
            IEnumerable<IWorkspaceObject> nav = mObjectAdapter.Load();
            IEnumerable<IWorkspaceObject> scs = mVCCClient.GetItems();

            // determine the objects which needs to be synced:
            // - objects which are both in nav and tfs 
            // - objects which are locked in nav (in order to determine deleted items deleted from tfs)
            IEnumerable<IWorkspaceObject> result = CompareTfsWithNav(nav, scs).Union(CompareNavWithTfs(nav.Where(x => x.Locked), scs));

            //mConnection.AddToHistory(DoSync(result));           
            DoSync(result);

            mNavObjectDesigner.Compile();            
        }

        private IEnumerable<IWorkspaceObject> DoSync(IEnumerable<IWorkspaceObject> objects)
        {
            List<IWorkspaceObject> import = new List<IWorkspaceObject>();
            List<IWorkspaceObject> export = new List<IWorkspaceObject>();
            List<IWorkspaceObject> result = new List<IWorkspaceObject>();
            

            // export all items with sync action add
            Export(from o in objects where o.SyncAction.HasFlag(SyncAction.Add) select o);

            foreach (WorkspaceObject obj in objects)
            {
                if (obj.SyncAction.HasFlag(SyncAction.Add))
                {
                    //mNavObjectDesigner.Export(obj);
                    mVCCClient.Add(obj);
                    obj.ChangeType = ChangeType.Add;
                };
                if (obj.SyncAction.HasFlag(SyncAction.Checkout))
                {
                    mVCCClient.Checkout(obj);
                    obj.ChangeType = ChangeType.Edit;                    
                };
                if (obj.SyncAction.HasFlag(SyncAction.Unlock))
                {
                    obj.Unlock();                                        
                };
                if (obj.SyncAction.HasFlag(SyncAction.Lock))
                {
                    obj.Lock();
               
                };
                if (obj.SyncAction.HasFlag(SyncAction.Import))
                {
                    import.Add((WorkspaceObject)obj);                    
                };
                if (obj.SyncAction.HasFlag(SyncAction.Export))
                {
                    export.Add((WorkspaceObject)obj);
                }
                result.Add(obj);
                mConnection.AddToHistory(obj);
            }
            ImportNavObjects(import);                        
            mObjectAdapter.Save(objects.Where(x => x.SyncAction.HasFlag(SyncAction.Update)));
            Export(export);
            return result;
        }
      
        public IEnumerable<IWorkspaceObject> CompareNavWithTfs(IEnumerable<IWorkspaceObject> nav, IEnumerable<IWorkspaceObject> tfs)
        {
            List<IWorkspaceObject> objects = new List<IWorkspaceObject>();

            foreach (IWorkspaceObject n in nav.Except(tfs, new NavObjectComparer()))
            {
                var obj = GetObjectToSync(null, n);
                if (obj != null)
                    objects.Add(obj);
            }
            return objects;
        }

        public IEnumerable<IWorkspaceObject> CompareTfsWithNav(IEnumerable<IWorkspaceObject> nav, IEnumerable<IWorkspaceObject> tfs)
        {
            List<IWorkspaceObject> objects = new List<IWorkspaceObject>();
            var j = from t in tfs
                    join n in nav on new { t.Type, t.ID } equals new { n.Type, n.ID } into ps
                    from p in ps.DefaultIfEmpty()
                    select new { a = t, b = p };

            foreach (var i in j)
            {
                var obj = GetObjectToSync(i.a, i.b);
                if (obj != null)
                    objects.Add(obj);
            }
            return objects;
        }
        
        IWorkspaceObject GetObjectToSync(IWorkspaceObject tfs, IWorkspaceObject nav)
        {
            IWorkspaceObject obj = null;

            if (nav != null && tfs != null)
            {
                if (nav.Locked && ObjectIsChanged(nav)) 
                { 
                    obj = nav; obj.SyncAction = SyncAction.Export;
                    mConnection.Cache[nav.GetHashCode()] = nav.Timestamp;
                }
                if (nav.VersionLocal != tfs.VersionLocal && tfs.ChangeType != ChangeType.Delete)
                {
                    // the workspace and nav object have different versions.                                                 
                    obj = nav;
                    obj.SyncAction = SyncAction.Import | SyncAction.Update;
                    obj.Verbose("Nav version {@navversion} differs from workspace version {@workspaceversion} -> import and compile the workspace version", nav.VersionLocal, tfs.VersionLocal);                        
                }
                if (!nav.Locked && tfs.ChangeType != ChangeType.None && tfs.ChangeType != ChangeType.Delete)
                {
                    // the item has been checked out at tfs but has not been locked in NAV -> lock the item in NAV                                             
                    obj = nav;
                    obj.SyncAction = SyncAction.Lock | SyncAction.Import | SyncAction.Update;
                    obj.Verbose("Workspace has pending changes and the object is not locked in nav -> lock the object in nav");                    
                }
                if (nav.Locked && nav.ChangeType == ChangeType.None && tfs.ChangeType == ChangeType.None)
                {
                    // locked in nav but not in tfs -> checkout the item at tfs
                    obj = nav;
                    obj.SyncAction = SyncAction.Checkout | SyncAction.Export | SyncAction.Update;
                    obj.Verbose("Object is locked in nav and does not have a pending change -> checkout the object in tfs");                   
                }
                if (nav.Locked && nav.ChangeType != ChangeType.None && tfs.ChangeType == ChangeType.None)
                {
                    // item has been locked in nav but pending change has been canceled at tfs -> unlock the item.                        
                    obj = nav;
                    obj.SyncAction = SyncAction.Unlock | SyncAction.Import | SyncAction.Update;
                    obj.Verbose("Object is locked in nav and pending changes were removed -> unlock the object and import the workspace version");                    
                }
                if (obj != null)
                {
                    obj.VersionLocal = tfs.VersionLocal;
                    obj.ChangeType = tfs.ChangeType;
                }
            }
            else if (nav == null && tfs.ChangeType != ChangeType.Delete)
            {
                // new item in workspace -> import the item in nav
                obj = tfs;
                obj.Connection = mConnection;
                
                // Local Version = 0: 
                // - Change Type = None -> new item which is not downloaded : do nothing
                // - Change Type > None -> new item with pending change has been unshelved : import
                if ((tfs.VersionLocal > 0) || (tfs.VersionLocal == 0 && tfs.ChangeType != ChangeType.None))
                {
                    obj.SyncAction = SyncAction.Import | SyncAction.Update;
                    obj.Verbose("Object does exist in nav and does not have a pending deletion in the workspace -> import the item in nav");
                }                                
            }
            else if (tfs == null)
            {
                // item is not in the workspace and is not locked in nav -> add the item to tfs
                if (nav.ChangeType == ChangeType.None)
                {
                    obj = nav;
                    obj.ChangeType = ChangeType.Add;
                    obj.SyncAction = SyncAction.Add | SyncAction.Export | SyncAction.Update;
                    obj.Verbose("Object is locked in nav and does not exist in workspace -> add the item to the workspace");                    
                }
                else
                {
                    obj = nav;
                    obj.SyncAction = SyncAction.Unlock | SyncAction.Update;
                    // import the item if the change type was not add; in this case there is no local spec if pending changes are canceled
                    if (nav.ChangeType != ChangeType.Add)
                    {
                        obj.Verbose("Object is locked with change type {@ChangeType} -> unlock the item and import it in nav", nav.ChangeType);
                        obj.SyncAction = obj.SyncAction | SyncAction.Import | SyncAction.Update;
                    }
                    else 
                    {
                        obj.Verbose("Object is locked with change type {@ChangeType} -> unlock the item in nav", nav.ChangeType);
                    }                                        
                }
            }
            return obj;
        }

        private void ImportNavObjects(IEnumerable<IWorkspaceObject> objectsToSync)
        {
            mNavObjectDesigner.Import(objectsToSync);
        }

        private bool ObjectIsChanged(IWorkspaceObject obj)
        {
            return ((!mConnection.Cache.Contains(obj.GetHashCode())) || ((Int64)mConnection.Cache[obj.GetHashCode()] != obj.Timestamp));
        }

        private void Export(IEnumerable<IWorkspaceObject> objects)
        {
            if (objects.Count() > 0)
            {
                string fileName = Utilities.GetTempFileName();

                mNavObjectDesigner.ExportLockedObjects(fileName);

                var exported = Utilities.GetObjectsFromExportFile(mConnection, fileName);

                var exp = from o in objects
                          join e in exported on new { o.Type, o.ID } equals new { e.Item1.Type, e.Item1.ID } 
                          select new { Item = o, Content = e.Item2};
                                                                          
                foreach (var o in exp)
                {
                    WriteItemToFolder(o.Item, o.Content);
                }
                
                File.Delete(fileName);
            }
        }

        private void WriteItemToFolder(IWorkspaceObject obj, string content)
        {
            try
            {
                File.WriteAllText(obj.LocalSpec, content, Encoding.GetEncoding(Utilities.DYNAMICS_NAV_ENCODING));
                Log.Verbose("Export {@item} to workspace", obj.ItemName);
            }
            catch (UnauthorizedAccessException ex)
            {
                Log.Warning(ex, "Cannot export {@item} to the workspace.");
            }
        }
    }
}