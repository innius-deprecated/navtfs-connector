﻿using Microsoft.TeamFoundation.VersionControl.Client;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public class NavObjectSqlAdapter : INavObjectAdapter
    {
        private IConnection mConnection;

        public NavObjectSqlAdapter(IConnection sccConnection)
        {
            mConnection = sccConnection;
        }

        #region public methods
        public IEnumerable<IWorkspaceObject> Load()
        {
            using (SqlConnection connection = new SqlConnection(mConnection.ConnectionString))
            {
                // gets a list of all items which are currently version controled            
                SqlDataAdapter adapter = GetNavObjectDataAdapter(connection);

                DataTable table = new DataTable();
                adapter.Fill(table);

                List<WorkspaceObject> objects = new List<WorkspaceObject>();
                foreach (DataRow row in table.AsEnumerable())
                {
                    objects.Add(Row2NavObject(row));
                }
                return objects;
            }
        }

        public void Save(IEnumerable<IWorkspaceObject> navObjects)
        {
            if (navObjects.Count() > 0)
            {
                using (SqlConnection connection = new SqlConnection(mConnection.ConnectionString))
                {
                    SqlDataAdapter adapter = GetNavObjectDataAdapter(connection);
                    DataTable table = new DataTable();
                    adapter.Fill(table);

                    DataRow[] row;
                    foreach (WorkspaceObject obj in navObjects)
                    {
                        row = table.Select(string.Format("ID = {0} AND Type = {1}", obj.ID, (int)(obj.Type)));
                        if (row.Count() == 1)
                        {
                            NavObject2Row(row, obj);
                        }
                    }
                    adapter.Update(table);
                }
            }
        }

        public void EnableAdapter()
        {
            AddSqlTableColumns(mConnection.ConnectionString);
        }

        private void AddSqlTableColumns(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                AddSqlTableColumn(connection, "Object", "Tfs Local Version", 0);
                AddSqlTableColumn(connection, "Object", "Tfs Change Type", 1);
            }
        }

        private void AddSqlTableColumn(SqlConnection connection, string table, string column, int defaultValue)
        {
            using (SqlCommand command = new SqlCommand())
            {
                Log.Verbose("Check if column {@column} exists in table {@table}", column, table);
                command.Connection = connection;
                command.CommandText = string.Format(
                    "IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS " +
                    "WHERE TABLE_NAME = '{0}' AND COLUMN_NAME = '{1}') " +
                    "BEGIN " +
                        "ALTER TABLE {0} ADD [{1}] int default {2} NOT NULL " +
                    "END", table, column, defaultValue);
                command.ExecuteNonQuery();
                Log.Verbose("Column {@column} exists in table {@table}", column, table);
            }
        }

        #endregion

        #region private methods
        private WorkspaceObject Row2NavObject(DataRow row)
        {
            return new WorkspaceObject
            {
                Type = (NavObjectType)int.Parse(row["Type"].ToString()),
                ID = int.Parse(row["ID"].ToString()),
                Name = row["Name"].ToString(),
                Date = ParseNavDateTime(DateTime.Parse(row["Date"].ToString()), DateTime.Parse(row["Time"].ToString())),
                Locked = (bool)(int.Parse(row["Locked"].ToString()) == 1),
                LockedBy = row["Locked By"].ToString(),
                VersionLocal = string.IsNullOrEmpty(row["Tfs Local Version"].ToString()) ? 0 : Convert.ToInt32(row["Tfs Local Version"].ToString()),
                ChangeType = int.Parse(row["Tfs Change Type"].ToString()) == 0 ? ChangeType.None : (ChangeType)Convert.ToInt32(row["Tfs Change Type"].ToString()),
                Connection = mConnection,
                Timestamp = Math.Abs(BitConverter.ToInt64((byte[])row["timestamp"], 0))
            };
        }

        private DateTime ParseNavDateTime(DateTime date, DateTime time)
        {
            return date.AddHours(time.Hour).AddMinutes(time.Minute).AddSeconds(time.Second);
        }

        private void NavObject2Row(DataRow[] row, WorkspaceObject obj)
        {
            row[0]["Tfs Local Version"] = obj.VersionLocal;
            row[0]["Locked"] = obj.Locked;
            row[0]["Locked By"] = obj.LockedBy;
            row[0]["Tfs Change Type"] = (int)obj.ChangeType;
        }

        private SqlDataAdapter GetNavObjectDataAdapter(SqlConnection connection)
        {
            // check if Sql Columns are there                        
            SqlDataAdapter adapter = new SqlDataAdapter(
                "SELECT timestamp, Type, ID, Name, Date, Time,Locked, [Locked By], [Tfs Local Version], [Tfs Change Type] " +
                "FROM dbo.Object " +
                "WHERE Type > 0",
                connection);

            adapter.UpdateCommand = new SqlCommand(
                "UPDATE dbo.Object " +
                "SET [Tfs Local Version] = @LocalTfsVersion, Locked = @Locked, [Locked By] = @LockedBy, [Tfs Change Type] = @TfsChangeType " +
                "WHERE Type = @Type AND ID=@ID", connection);

            adapter.UpdateCommand.Parameters.Add(new SqlParameter { ParameterName = "@LocalTfsVersion", SqlDbType = System.Data.SqlDbType.Int, SourceColumn = "Tfs Local Version" });
            adapter.UpdateCommand.Parameters.Add(new SqlParameter { ParameterName = "@TfsChangeType", SqlDbType = System.Data.SqlDbType.Int, SourceColumn = "Tfs Change Type" });
            adapter.UpdateCommand.Parameters.Add(new SqlParameter { ParameterName = "@Locked", SqlDbType = System.Data.SqlDbType.TinyInt, SourceColumn = "Locked" });
            adapter.UpdateCommand.Parameters.Add(new SqlParameter { ParameterName = "@LockedBy", SqlDbType = System.Data.SqlDbType.VarChar, SourceColumn = "Locked By" });
            adapter.UpdateCommand.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = System.Data.SqlDbType.Int, SourceColumn = "Type" });
            adapter.UpdateCommand.Parameters.Add(new SqlParameter { ParameterName = "@ID", SqlDbType = System.Data.SqlDbType.Int, SourceColumn = "ID" });

            adapter.InsertCommand = adapter.UpdateCommand;

            return adapter;
        }
        #endregion
    }

}
