﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WindowsClient.Models;

namespace WindowsClient
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        private LicenseView _licenseView = new LicenseView();
        public About()
        {
            InitializeComponent();
            DataContext = _licenseView;
            _licenseView.Load();
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            var hyperlink = sender as Hyperlink;
            System.Diagnostics.Process.Start(hyperlink.NavigateUri.ToString());
        }

        private void Hyperlink_ShowLicenseInfo(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            var registration = new RegisterProduct();
            registration.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

       

    }
}
