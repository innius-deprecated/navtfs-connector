﻿using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TI.NavTfsConnector;
using TI.NavTfsConnector.NavObjectDesigner;

namespace WindowsClient
{
    public class Connection : BaseConnection, INotifyPropertyChanged
    {
        public Connection()
        {
            this.Status = ConnectionStatus.Stopped;
        }

        public void ShowErrorMessage()
        {
            MessageBox.Show(this.LastErrorMessage, "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void ShowLog()
        {
            var dlg = new ConnectionHistory(this);
            dlg.Show();
        }

        public void Design(IWorkspaceObject obj)
        {
            var designer = NavObjectDesignerFactory.GetObjectDesigner(this);
            designer.Design(obj);
        }

        #region properties
        private ConnectionStatus mStatus;
        public override ConnectionStatus Status
        {
            get
            { return mStatus; }
            set
            {
                mStatus = value;
                OnPropertyChanged("Status");
            }
        }

        public override string LastErrorMessage
        {
            get
            {
                return base.LastErrorMessage;
            }
            set
            {
                base.LastErrorMessage = value;
                OnPropertyChanged("LastErrorMessage");
            }
        }


        private string mName;
        public override string Name
        {
            get { return mName; }
            set
            {
                mName = value;
                OnPropertyChanged("Name");
            }
        }

        public override string ConnectionString
        {
            get
            {
                return base.ConnectionString;
            }
            set
            {
                base.ConnectionString = value;
                OnPropertyChanged("ConnectionString");
            }
        }

        public override string Database
        {
            get
            {
                return base.Database;
            }
            set
            {
                base.Database = value;
                OnPropertyChanged("Database");
            }
        }

        public override string NavClientFolder
        {
            get
            {
                return base.NavClientFolder;
            }
            set
            {
                base.NavClientFolder = value;
                OnPropertyChanged("NavClientFolder");
            }
        }

        public override string WorkspaceFolder
        {
            get
            {
                return base.WorkspaceFolder;
            }
            set
            {
                base.WorkspaceFolder = value;
                OnPropertyChanged("WorkspaceFolder");
            }
        }

        public override Uri ProjectCollection
        {
            get
            {
                return base.ProjectCollection;
            }
            set
            {
                base.ProjectCollection = value;
                OnPropertyChanged("ProjectCollection");
            }
        }

        public override NamingConvention ObjectNamingConvention
        {
            get
            {
                return base.ObjectNamingConvention;
            }
            set
            {
                base.ObjectNamingConvention = value;
                OnPropertyChanged("ObjectNamingConvention");
            }
        }

        public override string NamingExpression
        {
            get
            {
                return base.NamingExpression;
            }
            set
            {
                base.NamingExpression = value;
                OnPropertyChanged("NamingExpression");
            }
        } 

        ObservableCollection<IWorkspaceObject> mHistory = new ObservableCollection<IWorkspaceObject>();
        public ObservableCollection<IWorkspaceObject> History
        {
            get
            {
                return mHistory;
            }
        }
        
        public override void AddToHistory(IWorkspaceObject obj)
        {
            var now = DateTime.Now;
            if (obj.SyncAction != SyncAction.None)
            {
                obj.Date = now;
                mHistory.Add(obj);
            }
        }

        private Result _connectionStringVerified;
        public Result ConnectionStringVerified
        {
            get { return _connectionStringVerified; }
            set
            {
                _connectionStringVerified = value;
                OnPropertyChanged("ConnectionStringVerified");
            }
        }

        private Result _projectCollectionVerified;
        public Result ProjectCollectionVerified
        {
            get { return _projectCollectionVerified; }
            set
            {
                _projectCollectionVerified = value;
                OnPropertyChanged("ProjectCollectionVerified");
            }
        }

        private Result _workspaceVerified;
        public Result WorkspaceVerified
        {
            get { return _workspaceVerified; }
            set
            {
                _workspaceVerified = value;
                OnPropertyChanged("WorkspaceVerified");
            }
        }

        private Result _navClientFolderVerified;
        public Result NavClientFolderVerified
        {
            get { return _navClientFolderVerified; }
            set
            {
                _navClientFolderVerified = value;
                OnPropertyChanged("NavClientFolderVerified");
            }
        }
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propName)
        {
            var pc = PropertyChanged;
            if (pc != null)
                pc(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

        public Connection ShallowCopy()
        {
            return (Connection)this.MemberwiseClone();
        }

        async public void Verify()
        {
            this.ConnectionStringVerified = await VerifyDatabase();
            this.ProjectCollectionVerified = await VerifyProjectCollection();
            this.WorkspaceVerified = await VerifyWorkspaceAsync();
            this.NavClientFolderVerified = await VerifyNavClientFolderAsync();
        }        
        
        private Task<Result> VerifyNavClientFolderAsync()
        {
            return Task.Run(() =>
            {
                try
                {
                    if (!File.Exists(Path.Combine(this.NavClientFolder, "finsql.exe")))
                        throw new FileNotFoundException("This folder does not contain the Microsoft Dynamics NAV Development Environment executable.");
                    return Result.Ok;
                }
                catch (Exception ex)
                {
                    this.LastErrorMessage = ex.Message;
                }
                return Result.Failed;
            });
        }

        private Task<Result> VerifyWorkspaceAsync()
        {
            return Task.Run(() =>
            {
                try
                {
                    if (!Directory.Exists(this.WorkspaceFolder))
                        throw new DirectoryNotFoundException();
                    TfsVersionControl tfsClient = new TfsVersionControl(this);
                    tfsClient.VerifyWorkspace();
                    return Result.Ok;

                }
                catch (Exception ex)
                {
                    this.LastErrorMessage = ex.Message;
                }
                return Result.Failed;
            });
        }

        private Task<Result> VerifyProjectCollection()
        {
            return Task.Run(() =>
            {
                try
                {
                   TfsVersionControl tfsClient = new TfsVersionControl(this);
                   tfsClient.VerifyProjectCollection();
                   return Result.Ok;
                   
                }
                catch (Exception ex)
                {
                    this.LastErrorMessage = ex.Message;                   
                }
                return Result.Failed;
            });
        }

        private Task<Result> VerifyDatabase()
        {
            return Task.Run(() =>
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(this.ConnectionString))
                    {
                        INavObjectAdapter adapter = new NavObjectSqlAdapter(this);
                        adapter.EnableAdapter();
                        return Result.Ok;
                    }
                }
                catch (Exception ex)
                {
                    this.LastErrorMessage = ex.Message;
                    Log.Error(ex, "Could not update the database for connection {@Connection}", this.Name);                    
                }
                return Result.Failed;
            });

        }
    }

    public enum Result
    {
        Unknown,
        Ok,
        Failed
    }
}
