﻿using Serilog;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using TI.NavTfsConnector;


namespace WindowsClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<Connection> _Connections = new ObservableCollection<Connection>();
        private static object _configLock = new object();
        private Service _service = new Service();
        LicenseService _licenseService = new LicenseService();        

        public MainWindow()
        {
            var repository = new ConnectionFileRepository<Connection>();

            foreach (var c in repository.Load())
            {
                _Connections.Add((Connection)c);
            }
            BindingOperations.EnableCollectionSynchronization(_Connections, _configLock);

            InitializeComponent();
                                    
            StartupService();
        }

        void _service_OnErrorEventHandler(object sender, EventArgs e)
        {
            Action action = () =>
            {
                this.TaskbarItemInfo.Overlay = (DrawingImage)this.FindResource("WarningImage");
            };
            Dispatcher.BeginInvoke(action);        
        }

        private void StartupService()
        {
            _service.OnErrorEventHandler += _service_OnErrorEventHandler;
            var t = new Task(() =>
            {
                var result = _licenseService.CheckLicense();
                                
                switch (result.Item1)
                {
                    case LicenseStatus.Unregistered:
                        // show the product registration dialog                        
                        Action action = () =>
                        {
                            RegisterProduct dlg = new RegisterProduct();
                            bool? r = dlg.ShowDialog();
                            if (r ?? false)
                            {
                                StartupService();
                            }
                            else                             
                            {
                                MessageBox.Show("The product must be registered before the synchronization can be started", "License Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            }
                        };
                        Dispatcher.BeginInvoke(action);
                        break;
                    case LicenseStatus.Expired:
                        Log.Warning("License {@license} is expired.", TI.NavTfsConnector.LicenseContext.MyLicense);
                        MessageBox.Show(result.Item2, "License Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        _service.Stop();
                        break;
                    case LicenseStatus.Invalid:
                        Log.Warning("License {@license} is invalid {@licensemessage}.", TI.NavTfsConnector.LicenseContext.MyLicense, result.Item2);
                        MessageBox.Show(result.Item2, "License Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        _service.Stop();
                        break;
                    case LicenseStatus.Valid:                        
                        StartupSyncService();
                        break;
                    default:
                        break;
                }
            });
            t.Start();
        }

        private void StartupSyncService()
        {
            _service.Start(_Connections);
        }

        public ICollectionView ConfigurationCollection
        {
            get
            {
                var sortedCollection = CollectionViewSource.GetDefaultView(_Connections);
                sortedCollection.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
                return sortedCollection;
            }
        }

        private void RibbonButton_StartClick(object sender, RoutedEventArgs e)
        {
            SetStatus(ConnectionStatus.Ready);
        }

        private void RibbonButton_StopClick(object sender, RoutedEventArgs e)
        {
            SetStatus(ConnectionStatus.Stopped);
        }

        private void SetStatus(ConnectionStatus status)
        {
            this.TaskbarItemInfo.Overlay = null;
            var connection = (Connection)listView.SelectedItem;
            if (connection != null)
            {
                {
                    connection.Status = status;
                }
            }
        }

        private void RibbonButton_ClickAddNew(object sender, RoutedEventArgs e)
        {
            CreateNewConnection(false);
        }

        private void RibbonButton_ClickEdit(object sender, RoutedEventArgs e)
        {
            // edit the page 
            var connection = (Connection)listView.SelectedItem;
            var dlg = new ConnectionWindow(connection);
            dlg.ShowDialog();
        }

        private void RibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            var connection = (Connection)listView.SelectedItem;
            connection.ShowLog();
        }

        private void RibbonButton_Click_Copy(object sender, RoutedEventArgs e)
        {
            CreateNewConnection(true);
        }

        private void RibbonButton_Click_Delete(object sender, RoutedEventArgs e)
        {
            var connection = (Connection)listView.SelectedItem;
            if (MessageBox.Show(string.Format("Delete Connection {0}?", connection.Name), Application.Current.Resources["AppTitle"].ToString(), MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                var repository = new ConnectionFileRepository<Connection>();
                repository.Delete(connection);
                _Connections.Remove(connection);
            }
        }

        private void CreateNewConnection(bool clone)
        {
            var connection = new Connection();
            if (clone)
            {
                connection = ((Connection)listView.SelectedItem).ShallowCopy();
                connection.Status = ConnectionStatus.Stopped;
                connection.Name = string.Format("{0}_Copy", connection.Name);
            }
            var dlg = new ConnectionWindow(connection);
            dlg.ShowDialog();
            connection = (Connection)dlg.DataContext;
            if (!_Connections.Contains(connection))
                _Connections.Add(connection);
        }

        private void RibbonButton_Click_About(object sender, RoutedEventArgs e)
        {
            var about = new About();
            about.Show();
        }       

        private void RibbonButton_Click_ShowError(object sender, RoutedEventArgs e)
        {
            var connection = ((Connection)listView.SelectedItem);
            connection.ShowErrorMessage();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {            
            e.Cancel = true;
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private void ExitApplication_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void RibbonButton_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://tfsfordynamicsnav.uservoice.com/");
        }
    }
}
