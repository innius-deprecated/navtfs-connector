﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TI.NavTfsConnector;
using WindowsClient.Models;

namespace WindowsClient
{
    /// <summary>
    /// Interaction logic for RegisterProduct.xaml
    /// </summary>
    public partial class RegisterProduct : Window
    {
        private LicenseView _license = new LicenseView();
        private LicenseService _service = new LicenseService();

       

        public RegisterProduct()
        {
            _license.Load();
            DataContext = _license;
            InitializeComponent();
            _license.LicenseCheck();
        }

        private void Button_ClickOK(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Button_ClickSelectLicense(object sender, RoutedEventArgs e)
        {
            // select the nav license 
            _license.LoadFromDynamicsNavLicense();
        }       
    }

    public class LicenseStatusConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            var status = (LicenseStatus)value;
            switch ((string)parameter)
            {
                case "statusmessage":
                    return status == LicenseStatus.Valid ? Visibility.Collapsed : Visibility.Visible;                    
                case "status_ok":
                    return status == LicenseStatus.Valid ? Visibility.Visible : Visibility.Collapsed;
                case "status_failed":
                    return ((status == LicenseStatus.Invalid) || (status == LicenseStatus.Unregistered)) ? Visibility.Visible : Visibility.Collapsed;                                   
            }                        
            return Visibility.Visible;            
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
