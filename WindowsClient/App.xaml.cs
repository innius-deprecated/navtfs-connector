﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TI.NavTfsConnector.Logging;
using WindowsClient.Models;

namespace WindowsClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            LogHelper.ConfigureLogger(Application.Current.Resources["AppId"].ToString());
            
            base.OnStartup(e);
        }
    }

  
}
