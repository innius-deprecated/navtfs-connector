﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using TI.NavTfsConnector;

namespace WindowsClient.Models
{
    public class LicenseView : INotifyPropertyChanged
    {
        private ILicense _license = new TI.NavTfsConnector.License();
        public ICommand RegisterLicenseCommand { get; private set; }

        public LicenseView()
        {
            RegisterLicenseCommand = new SimpleCommand
            {
                ExecuteDelegate = x => ConfirmRegistration(),
                CanExecuteDelegate = x => RegistrationAllowed()
            };  
        }

        private bool RegistrationAllowed()
        {
            return !string.IsNullOrWhiteSpace(this.LicenseNumber) && !String.IsNullOrWhiteSpace(this.Name) && !String.IsNullOrWhiteSpace(this.Email) && this.LicenseTermsAccepted;
        }
        #region properties
        public string LicenseNumber
        {
            get { return _license.LicenseNumber; }
            set
            {
                _license.LicenseNumber = value;
                OnPropertyChanged("LicenseNumber");
            }
        }

        public string LicensedTo
        {
            get { return _license.LicensedTo; }
            set
            {
                _license.LicensedTo = value;
                OnPropertyChanged("LicensedTo");
            }
        }

        public bool LicenseTermsAccepted
        {
            get
            { return _license.LicenseTermsAccepted; }
            set
            {
                _license.LicenseTermsAccepted = value;
                OnPropertyChanged("LicenseTermsAccepted");
            }
        }

        public string Name
        {
            get
            { return _license.Name; }
            set
            {
                _license.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Email
        {
            get
            { return _license.Email; }
            set
            {
                _license.Email = value;
                OnPropertyChanged("Email");
            }
        }

        public LicenseType LicenseType
        {
            get { return _license.LicenseType; }
            set
            {
                _license.LicenseType = value;
                OnPropertyChanged("LicenseType");
            }
        }

        public bool Expired
        {
            get { return _license.Expired; }
            set
            {
                _license.Expired = value;
                OnPropertyChanged("Expired");
            }
        }

        public string Version
        {
            get
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("Version {0}.{1}", version.Major, version.Minor);
            }
        }

        public string BuildNumber
        {
            get
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
            }
        }

        public LicenseStatus LicenseStatus
        {
            get
            {
                return _license.Status;
            }
            set
            {
                _license.Status = value;
                OnPropertyChanged("LicenseStatus");
            }
        }

        public string StatusMessage
        {
            get
            {
                return _license.StatusMessage;
            }
            set
            {
                _license.StatusMessage = value;
                OnPropertyChanged("StatusMessage");
            }
        }

        public Uri SoftwareLicenseAgreementUrl
        {
            get 
            {
                string appDir = Environment.CurrentDirectory;
                return new Uri(appDir + "/resources/SoftwareLicenseTerms.html");                
            }
        }
        #endregion

        public ILicense FromFlfFile(string fileName)
        {
            var service = new LicenseService();
            return service.LoadFromDynamicsNavLicense(_license, fileName);
        }

        public void ConfirmRegistration()
        {
            var service = new LicenseService();
            service.RegisterLicense(_license);
        }

        public void Load()
        {
            var service = new LicenseService();
            _license = service.Load();
        }

        public void LicenseCheck()
        {
            var t = new Task(() =>
            {
                var service = new LicenseService();
                service.CheckLicense();
                this.LicenseStatus = TI.NavTfsConnector.LicenseContext.MyLicense.Status;
                this.StatusMessage = TI.NavTfsConnector.LicenseContext.MyLicense.StatusMessage;
            });
            t.Start();
        }

        public void LoadFromDynamicsNavLicense()
        {                        
            var fileName = SelectFile();
            if (!string.IsNullOrEmpty(fileName))
            {
                var service = new LicenseService();
                _license = new TI.NavTfsConnector.License();
                service.LoadFromDynamicsNavLicense(_license, fileName);
                this.LicensedTo = _license.LicensedTo;
                this.LicenseNumber = _license.LicenseNumber;
                this.LicenseTermsAccepted = false;
                this.StatusMessage = string.Empty;
            }
        }

        private string SelectFile()
        {
            var dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Title = "Select Microsoft Dynamics NAV License File";
            dialog.Filter = "Microsoft Dynamics NAV License File | *.flf";
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                return dialog.FileName;
            }
            return "";
        }

        public event PropertyChangedEventHandler PropertyChanged;        

        protected virtual void OnPropertyChanged(string propName)
        {
            var pc = PropertyChanged;
            if (pc != null)
                pc(this, new PropertyChangedEventArgs(propName));
        }

    }


}
