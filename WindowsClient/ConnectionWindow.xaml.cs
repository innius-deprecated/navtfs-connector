﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TI.NavTfsConnector;

namespace WindowsClient
{
    /// <summary>
    /// Interaction logic for Connection.xaml
    /// </summary>
    public partial class ConnectionWindow : Window
    {
        public ConnectionWindow(IConnection connection)
        {
            DataContext = connection;
            InitializeComponent();
        }

        private void RibbonButton_ClickSave(object sender, RoutedEventArgs e)
        {
            // save the record
            var repository = new ConnectionFileRepository<Connection>();
            repository.Update((Connection)DataContext);
            this.Close();
        }

        private void RibbonButton_StartClick(object sender, RoutedEventArgs e)
        {
            SetStatus(ConnectionStatus.Ready);
        }

        private void RibbonButton_StopClick(object sender, RoutedEventArgs e)
        {
            SetStatus(ConnectionStatus.Stopped);
        }

        private void SetStatus(ConnectionStatus status)
        {
            var connection = (Connection)DataContext;
            if (connection != null)
            {
                {
                    connection.Status = status;
                }
            }
        }

        private void ButtonClick_ConfigureTeamProjectCollection(object sender, RoutedEventArgs e)
        {
            var connection = (Connection)DataContext;
            var projectUrl = TeamProjectHelper.SelectTeamProject();
            if (projectUrl != null)
                connection.ProjectCollection = projectUrl;
        }

        private void ButtonClick_ConfigureDatabase(object sender, RoutedEventArgs e)
        {
            var connection = (Connection)DataContext;
            var connectionString = SqlDataConnectionDialog.ShowDialog(connection.ConnectionString);
            if (!string.IsNullOrEmpty(connectionString))
            {
                connection.ConnectionString = connectionString;                
            }
        }

        private void ButtonClick_BrowseNavClientFolder(object sender, RoutedEventArgs e)
        {
            var connection = (Connection)DataContext;
            var folderName = SelectFolder(
                string.IsNullOrWhiteSpace(connection.NavClientFolder) ? Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) : connection.NavClientFolder,
                "Select Microsoft Dynamics NAV Client Folder");
            if (!string.IsNullOrEmpty(folderName))
            {
                connection.NavClientFolder = folderName;
            }
        }
      
        private string SelectFolder(string path, string description)
        {            
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();            
            dialog.SelectedPath = path;
            dialog.UseDescriptionForTitle = true;
            dialog.Description = description;            
            
            if ((bool)dialog.ShowDialog())
            {
                return dialog.SelectedPath;
            }            
            return "";
        }

        private void RibbonButton_Click(object sender, RoutedEventArgs e)
        {
            // check the database 
            var connection = (Connection)DataContext;
            connection.Verify();
        }

        private void ButtonClick_BrowseWorkspaceFolder(object sender, RoutedEventArgs e)
        {
            var connection = (Connection)DataContext;
            var folderName = SelectFolder(connection.WorkspaceFolder, "Select the Team Foundation Workspace Folder");
            if (!string.IsNullOrEmpty(folderName))
            {
                connection.WorkspaceFolder = folderName;
            }
        }

        private void RibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            var connection = (Connection)DataContext;
            connection.ShowLog();            
        }       
    }
    
    public sealed class MethodToValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var methodName = parameter as string;
            if (value == null || methodName == null)
                return value;
            var methodInfo = value.GetType().GetMethod(methodName, new Type[0]);
            if (methodInfo == null)
                return value;
            return methodInfo.Invoke(value, new object[0]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }
    }    
}
