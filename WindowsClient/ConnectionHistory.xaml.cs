﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TI.NavTfsConnector;

namespace WindowsClient
{
    /// <summary>
    /// Interaction logic for ConnectionHistory.xaml
    /// </summary>
    public partial class ConnectionHistory : Window
    {
        private ObservableCollection<IWorkspaceObject> _history = new ObservableCollection<IWorkspaceObject>();
        private static object _configLock = new object();
        private static Connection _connection;
        public ConnectionHistory(Connection connection)
        {
            _connection = connection;
            _history = connection.History;
            BindingOperations.EnableCollectionSynchronization(_history, _configLock);            
            InitializeComponent();
        }

        public ICollectionView ConfigurationCollection
        {
            get
            {
                var sortedCollection = CollectionViewSource.GetDefaultView(_history);
                sortedCollection.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
                return sortedCollection;
            }
        }

        private void RibbonButton_Click(object sender, RoutedEventArgs e)
        {
            var obj = (IWorkspaceObject)listView.SelectedItem;
            _connection.Design(obj);
        }    
 
        
    }
}
