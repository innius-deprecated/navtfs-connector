﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.ConnectionUI;

namespace WindowsClient
{
    public static class SqlDataConnectionDialog
    {
        public static string ShowDialog(string connectionString)
        {
            DataConnectionDialog dcd = new DataConnectionDialog();           
            
            dcd.DataSources.Add(DataSource.SqlDataSource);
            dcd.ConnectionString = connectionString;

            if (DataConnectionDialog.Show(dcd) == System.Windows.Forms.DialogResult.OK)
            {                
                return dcd.ConnectionString;
            }
            return string.Empty;
        }
        
    }
}
