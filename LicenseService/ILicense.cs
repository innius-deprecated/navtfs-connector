﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public enum LicenseType
    {
        Demo,
        Light,
        Advanced
    }

    public enum LicenseStatus
    {
        Unregistered,
        Expired,
        Invalid,
        Valid
    }

    public interface ILicense
    {
        string LicenseNumber { get; set; }

        string LicensedTo { get; set; }

        bool LicenseTermsAccepted { get; set; }

        string Name { get; set; }

        string Email { get; set; }

        LicenseType LicenseType { get; set; }

        DateTime ValidUntil { get; set; }

        bool Expired { get; set; }

        bool Active { get; set; }

        LicenseStatus Status { get; set; }
        string StatusMessage{ get; set; }
    }
}
