﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{


    public class License : ILicense
    {
        public string LicenseNumber { get; set; }

        public string LicensedTo { get; set; }

        public bool LicenseTermsAccepted { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public LicenseType LicenseType { get; set; }

        public bool Expired { get; set; }

        public DateTime ValidUntil { get; set; }

        public bool Active { get; set; }

        public LicenseStatus Status { get; set; }

        public string StatusMessage { get; set; }
    }
}
