﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TI.NavTfsConnector
{
    public class LicenseService
    {

        private const string VENDORID = "To-Increase";
        private const string VENDORNAME = "To-Increase B.V.";
        private const string PRODUCTID = "NAVTFSCONNECTOR";
        private const string PRODUCTNAME = "Visual Studio Source Control for Microsoft Dynamics NAV";

        public LicenseService()
        {
            if (LicenseContext.MyLicense == null)
            {
                LicenseContext.MyLicense = new License();
            }
        }

        public ILicense GetRegisteredLicense(ILicense license)
        {
            return license;
        }

        public Tuple<LicenseStatus, String> CheckLicense()
        {
            LicenseContext.MyLicense = Load();
            CheckLicense(LicenseContext.MyLicense);
            return new Tuple<LicenseStatus, string>(LicenseContext.MyLicense.Status, LicenseContext.MyLicense.StatusMessage);
        }

        public void CheckLicense(ILicense license)
        {
            var registeredLicense = GetRegisteredLicense(license);
            if (registeredLicense == null)
            {
                license.Status = LicenseStatus.Unregistered;
            }
            else
            {
                license.Status = LicenseStatus.Valid;
            }
        }

        /// <summary>
        /// Load the License Number and Namfe from a Dynamics NAV license file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ILicense LoadFromDynamicsNavLicense(ILicense license, string fileName)
        {
            string line;
            using (var s = new StreamReader(fileName, Encoding.Default))
            {
                int i = 0;
                while ((line = s.ReadLine()) != null && i < 5)
                {
                    i++;
                    switch (i)
                    {
                        case 4:
                            license.LicenseNumber = line.Split(':')[1].Trim();
                            break;
                        case 5:
                            license.LicensedTo = line.Split(':')[1].Trim();
                            break;
                        default:
                            break;
                    }
                }
            }
            return license;
        }

        public void RegisterLicense(ILicense license)
        {
            Save(license);
        }

        /// <summary>
        /// Writes a json string of the license to the user directory
        /// </summary>
        /// <returns>the saved license</returns>
        private void Save(ILicense license)
        {
            // save the current object as a json string to the user temp folder
            string json = JsonConvert.SerializeObject(license);
            File.WriteAllText(GetLicenseFilePath(), json);
        }

        public ILicense Load()
        {
            var license = new License();
            if (File.Exists(GetLicenseFilePath()))
            {
                license = JsonConvert.DeserializeObject<License>(File.ReadAllText(GetLicenseFilePath()));
            }
            return license;
        }


        public static string GetLicenseFilePath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "license.txt");
        }
    }
}
