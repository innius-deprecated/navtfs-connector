﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using TI.NavTfsConnector;
using Xunit;

namespace NavTfsConnector.Test
{
    public class ConnectionFileRepositoryTest
    {
        [Fact]
        public void SaveLoadSettings()
        {
            // arrange            
            var repostory = GetTestRespository();
            var connection = GetTestConnection();
            repostory.Update(connection);
            
            // act
            var result = repostory.Load().First(x => x.Name == connection.Name);

            // assert
            Assert.Equal(connection.Name, result.Name);
            Assert.Equal(connection.ConnectionString, result.ConnectionString);
            Assert.Equal(connection.ObjectNamingConvention, result.ObjectNamingConvention);
            Assert.Equal(connection.NamingExpression, result.NamingExpression);
        }        

        [Fact]
        public void ReadNamingExpressionFromExistingConnection()
        {
            // arrange
            var repostory = GetTestRespository();
            var connection = GetTestConnection();
            connection.NamingExpression = null;
            connection.ObjectNamingConvention = NamingConvention.Default;
            repostory.Update(connection);

            // act
            var result = repostory.Load().First(x => x.Name == connection.Name);

            // arrange
            Assert.Equal(connection.Name, result.Name);
            Assert.Equal(connection.ObjectNamingConvention, result.ObjectNamingConvention);
            Assert.Equal(string.Empty, result.NamingExpression);
        }

        private ConnectionFileRepository<Connection> GetTestRespository()
        {
            return new ConnectionFileRepository<Connection>(Path.GetTempPath());            
        }

        private IConnection GetTestConnection()
        {
            return new Connection()
            {
                Name = "testconnection",
                ConnectionString = "Data Source=localhost;Initial Catalog=\"Demo Database NAV (7-1)\";Integrated Security=True",
                ObjectNamingConvention = NamingConvention.Custom,
                NamingExpression = "{Type:4}-{ID:6}.txt"
            };
        }
    }
}
