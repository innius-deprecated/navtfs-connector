﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TI.NavTfsConnector;
using Xunit;
using Xunit.Extensions;

namespace NavTfsConnector.Test
{
    public class FileNameTest
    {
        [Theory]
        [InlineData("Codeunit80.txt", NavObjectType.Codeunit, 80)]
        [InlineData("Table80.txt", NavObjectType.Table, 80)]
        [InlineData("Page80.txt", NavObjectType.Page, 80)]
        [InlineData("Report80.txt", NavObjectType.Report, 80)]
        [InlineData("XmlPort80.txt", NavObjectType.XmlPort, 80)]
        [InlineData("Query80.txt", NavObjectType.Query, 80)]
        [InlineData("Dataport80.txt", NavObjectType.Dataport, 80)]
        [InlineData("MenuSuite80.txt", NavObjectType.MenuSuite, 80)]
        public void ParseFileNameDefault(string fileName, NavObjectType type, int id)
        {
            ParseFileName(fileName, type, id, NamingConvention.Default);
        }

        [Theory]
        [InlineData("COD80.txt", NavObjectType.Codeunit, 80)]
        [InlineData("COD000080.txt", NavObjectType.Codeunit, 80)]
        [InlineData("TAB80.txt", NavObjectType.Table, 80)]
        [InlineData("PAG80.txt", NavObjectType.Page, 80)]
        [InlineData("REP80.txt", NavObjectType.Report, 80)]
        [InlineData("XML80.txt", NavObjectType.XmlPort, 80)]
        [InlineData("QUE80.txt", NavObjectType.Query, 80)]
        [InlineData("DAT80.txt", NavObjectType.Dataport, 80)]
        [InlineData("MEN80.txt", NavObjectType.MenuSuite, 80)]
        public void ParseFileNameClassic(string fileName, NavObjectType type, int id)
        {
            ParseFileName(fileName, type, id, NamingConvention.Classic);
        }
        
        [Theory]
        [InlineData("CODE-000080.txt", NavObjectType.Codeunit, 80, "{Type:4}-{ID:5}.txt")]
        [InlineData("Codeunit_000080.txt", NavObjectType.Codeunit, 80, "{Type}_{ID:5}.txt")]
        public void ParseFileNameWithFormatString(string fileName, NavObjectType type, int id, string formatExpr)
        {
            ParseFileName(fileName, type, id, NamingConvention.Custom, formatExpr);
        }

        private void ParseFileName(string fileName, NavObjectType type, int id, NamingConvention namingConvention)
        {
            ParseFileName(fileName, type, id, namingConvention, string.Empty);
        }
        
        private void ParseFileName(string fileName, NavObjectType type, int id, NamingConvention namingConvention, string format)
        {
            // arrange
            var tfs = new Mock<IVersionControlSystem>();
            var lst = new List<Tuple<string, int, ChangeType>>()
            {
                new Tuple<string,int,ChangeType>(fileName,0,ChangeType.None)
            };
            tfs.Setup(x => x.GetItems()).Returns(lst);

            var con = new Connection();
            con.ObjectNamingConvention = namingConvention;
            con.NamingExpression = format;
            var vcs = new VersionControlClient(con, tfs.Object);

            // act
            var result = vcs.GetItems();

            // assert
            Assert.Equal(type, result.First().Type);
            Assert.Equal(id, result.First().ID);
        }
                
        [Theory]
        [InlineData(NavObjectType.Codeunit, 80,"Codeunit80.txt")]
        public void GenerateFileNameDefault(NavObjectType type, int id, string fileName)
        {
            GenerateFileNameTest(type, id, fileName, NamingConvention.Default, string.Empty);
        }

        [Theory]
        [InlineData(NavObjectType.Codeunit, 80, "COD80.txt")]
        public void GenerateFileNameClassic(NavObjectType type, int id, string fileName)
        {
            GenerateFileNameTest(type, id, fileName, NamingConvention.Classic, string.Empty);
        }

        [Theory]
        [InlineData(NavObjectType.Codeunit, 80, "CODE0000000080.txt", "{TYPE:4}{ID:10}.txt")]
        [InlineData(NavObjectType.Codeunit, 80, "code0000000080.txt", "{type:4}{ID:10}.txt")]
        [InlineData(NavObjectType.Codeunit, 80, "Code0000000080.txt", "{Type:4}{ID:10}.txt")]
        [InlineData(NavObjectType.Codeunit, 80, "Code-0000000080.txt", "{Type:4}-{ID:10}.txt")]
        [InlineData(NavObjectType.Codeunit, 80, "Codeunit_0000000080.txt", "{Type}_{ID:10}.txt")]
        public void GenerateFileNameCustom(NavObjectType type, int id, string fileName, string format)
        {
            GenerateFileNameTest(type, id, fileName, NamingConvention.Custom, format);
        }

        
        public void GenerateFileNameTest(NavObjectType type, int id, string fileName, NamingConvention namingConvention, string format)
        {
            // arrange
            var con = new Connection();
            con.ObjectNamingConvention = namingConvention;
            con.NamingExpression = format;
            var obj = new WorkspaceObject() { ID = id, Type = type, Connection = con};

            // act
            var result = obj.ItemName;

            // assert
            Assert.Equal(fileName, result);
        }
    }
}
