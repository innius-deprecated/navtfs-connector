﻿using Moq;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using TI.NavTfsConnector;
using TI.NavTfsConnector.NavObjectDesigner;
using Xunit;
using Xunit.Extensions;

namespace NavTfsConnector.Test
{
    public class ConnectorTest
    {
        [Fact]
        public void LockNavItemNotInTfs()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, Locked = true };

            // act
            var result = RunTest(nav, null);

            // assert
            Assert.Equal(SyncAction.Add | SyncAction.Export | SyncAction.Update, result.SyncAction);
            Assert.True(result.Locked);
        }


        [Fact]
        public void LockNavItemInTfs()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, Locked = true };
            var tfs = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1 };

            // act
            var result = RunTest(nav, tfs);

            // assert
            Assert.Equal(SyncAction.Checkout | SyncAction.Export | SyncAction.Update, result.SyncAction);
            Assert.True(result.Locked);
            Assert.Equal(ChangeType.Edit, result.ChangeType);
        }

        [Fact]
        public void GetTfsItemNotInNav()
        {
            // arrange
            var tfs = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1 };

            // act
            var result = RunTest(null, tfs);

            // assert
            Assert.Equal(SyncAction.Import | SyncAction.Update, result.SyncAction);
            Assert.False(result.Locked);
            Assert.Equal(tfs.VersionLocal, result.VersionLocal);
        }
        
        [Fact]
        public void GetTfsItemInNav()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1 };
            var tfs = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 2 };

            // act
            var result = RunTest(nav, tfs);

            // assert
            Assert.Equal(SyncAction.Import | SyncAction.Update, result.SyncAction);
            Assert.False(result.Locked);
            Assert.Equal(tfs.VersionLocal, result.VersionLocal);
        }

        [Fact]
        public void CheckOutTfsItemNotInNav()
        {
            // arrange
            var tfs = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1 , ChangeType = ChangeType.Edit};

            // act
            var result = RunTest(null, tfs);

            // assert
            Assert.Equal(SyncAction.Import | SyncAction.Update, result.SyncAction);
            Assert.False(result.Locked);
            Assert.Equal(tfs.VersionLocal, result.VersionLocal);
        }

        [Fact]
        public void LockedNavObjectCheckedOutInTfs()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1, Locked = true, Timestamp = 1 };
            var tfs = new WorkspaceObject() { Type = nav.Type, ID = nav.ID, VersionLocal = nav.VersionLocal, ChangeType = ChangeType.Edit};
            
            // act
            var result = RunTest(nav, tfs);

            // assert
            Assert.Equal(SyncAction.Export, result.SyncAction);
        }

        [Fact]
        public void UnChangedLockedNavObjectCheckedOutInTfs()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1, Locked = true, Timestamp = 1 };
            var tfs = new WorkspaceObject() { Type = nav.Type, ID = nav.ID, VersionLocal = nav.VersionLocal, ChangeType = ChangeType.Edit };
            var con = new Connection();
            RunTest(con, nav, tfs);
            
            // act
            var result = RunTest(con, nav, tfs);

            // assert
            Assert.Equal(SyncAction.Export, result.SyncAction);
            Assert.True(result.Locked);
            Assert.Equal(1, con.History.Count());
        }

        [Fact]
        public void ChangedLockedNavObjectCheckedOutInTfs()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1, Locked = true, Timestamp = 1 };
            var tfs = new WorkspaceObject() { Type = nav.Type, ID = nav.ID, VersionLocal = nav.VersionLocal, ChangeType = ChangeType.Edit };
            var con = new Connection();
            RunTest(con, nav, tfs);
            nav.Timestamp = 2; // change the time stamp 

            // act
            RunTest(con, nav, tfs);

            // assert
            Assert.Equal(SyncAction.Export, con.History.Last().SyncAction);            
            Assert.Equal(2, con.History.Count());
        }

        [Fact]
        public void UndoPendingChanges()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1, Locked = true, Timestamp = 1 , ChangeType = ChangeType.Edit};
            var tfs = new WorkspaceObject() { Type = nav.Type, ID = nav.ID, VersionLocal = nav.VersionLocal, ChangeType = ChangeType.None };

            // act
            var result = RunTest(nav, tfs);

            // assert
            Assert.Equal(SyncAction.Import | SyncAction.Unlock | SyncAction.Update, result.SyncAction);
        }

        [Fact]
        public void CheckOutExistingItemInTfs()
        {
            // arrange
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1, Locked = false };
            var tfs = new WorkspaceObject() { Type = nav.Type, ID = nav.ID, VersionLocal = nav.VersionLocal, ChangeType = ChangeType.Edit };

            // act
            var result = RunTest(nav, tfs);

            // assert
            Assert.Equal(SyncAction.Lock | SyncAction.Import | SyncAction.Update, result.SyncAction);
        }

        [Fact]
        public void DeleteItemFromTfs()
        {

        }

        [Theory]
        [InlineData(true)]
        [InlineData(true)]
        public void DeleteItemFromNav(bool locked)
        { 
            // arrange           
            var nav = new WorkspaceObject() { Type = NavObjectType.Codeunit, ID = 1, VersionLocal = 1, Locked = locked};
            var tfs = new WorkspaceObject() { Type = nav.Type, ID = nav.ID, VersionLocal = nav.VersionLocal, ChangeType = ChangeType.Edit };

            // act
            var result = RunTest(nav, tfs);
        }


        private IWorkspaceObject RunTest(WorkspaceObject nav, WorkspaceObject tfs)
        {
            return RunTest(new Connection(), nav, tfs);
        }

        private IWorkspaceObject RunTest(Connection c, WorkspaceObject nav, WorkspaceObject tfs)
        {
            // moq the object designer
            var objAdapter = new Mock<INavObjectAdapter>();

            objAdapter.Setup(x => x.Load()).Returns(nav == null ? new List<WorkspaceObject>() : new List<WorkspaceObject>() { nav });

            var tfsClient = new Mock<IVersionControlClient>();
            tfsClient.Setup(x => x.GetItems()).Returns(tfs == null ? new List<WorkspaceObject>() : new List<WorkspaceObject>() { tfs });
            
            var objDesigner = new Mock<INavObjectDesigner>();

            // moq the tfs client
            var connector = new TI.NavTfsConnector.Connector(c, objDesigner.Object, tfsClient.Object, objAdapter.Object);

            // act
            connector.Synchronize();

            CallContext.FreeNamedDataSlot(typeof(LogContext).FullName);

            return c.History.First();
        }
    }

    internal class Connection : BaseConnection
    {
        public List<IWorkspaceObject> History = new List<IWorkspaceObject>();

        public override void AddToHistory(IWorkspaceObject obj)
        {
            History.Add(obj);    
        }
    }

}
